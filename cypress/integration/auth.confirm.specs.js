/// <reference types="Cypress" />

context('Actions', () => {
    beforeEach(() => {
      cy.visit('http://localhost:9003/#/SignUp')
    })
  
    // https://on.cypress.io/interacting-with-elements
  
    it('Name field >> .type() - type into a DOM element', () => {
      // https://on.cypress.io/type
      cy.get('#name')
        .type('Nuevo Usuario').should('have.value', 'Nuevo Usuario')
  
        // .type() with special character sequences
        .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
        .type('{del}{selectall}{backspace}')
  
        // .type() with key modifiers
        .type('{alt}{option}') //these are equivalent
        .type('{ctrl}{control}') //these are equivalent
        .type('{meta}{command}{cmd}') //these are equivalent
        .type('{shift}')
  
        // Delay each keypress by 0.1 sec
        .type('N', { delay: 100 })
        .closest('.has-error')
        .find('.ant-form-explain')
        .should('contain', 'name must be at least 2 characters')
    })

    it('LastName field >> .type() - type into a DOM element', () => {
      // https://on.cypress.io/type
      cy.get('#lastName')
        .type('Nuevo Usuario Apellido').should('have.value', 'Nuevo Usuario Apellido')
  
        // .type() with special character sequences
        .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
        .type('{del}{selectall}{backspace}')
  
        // .type() with key modifiers
        .type('{alt}{option}') //these are equivalent
        .type('{ctrl}{control}') //these are equivalent
        .type('{meta}{command}{cmd}') //these are equivalent
        .type('{shift}')

        .closest('.has-error')
        .find('.ant-form-explain')
        .should('contain', 'Please enter your Apellido');

        cy.get('#lastName')
        .type('p')
        .closest('.has-error')
        .find('.ant-form-explain')
        .should('contain', 'lastName must be at least 2 characters')
    })

    it('Email Field >> .type() - type into a DOM element', () => {
      // https://on.cypress.io/type
      cy.get('#email')
        .type('fake@email.com').should('have.value', 'fake@email.com')
  
        // .type() with special character sequences
        .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
        .type('{del}{selectall}{backspace}')
  
        // .type() with key modifiers
        .type('{alt}{option}') //these are equivalent
        .type('{ctrl}{control}') //these are equivalent
        .type('{meta}{command}{cmd}') //these are equivalent
        .type('{shift}')

        .should('have.attr', 'type',  'email')

        .closest('.has-error')
        .find('.ant-form-explain')
        .should('contain', 'Please enter your email')

    })

    it('Password Field >> .type() - type into a DOM element', () => {
        // https://on.cypress.io/type
        cy.get('#password')
          .type('12345678').should('have.value', '12345678')
    
          // .type() with special character sequences
          .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
          .type('{del}{selectall}{backspace}')
    
          // .type() with key modifiers
          .type('{alt}{option}') //these are equivalent
          .type('{ctrl}{control}') //these are equivalent
          .type('{meta}{command}{cmd}') //these are equivalent
          .type('{shift}')
    
          .should('have.attr', 'type',  'password')
          
          .closest('.has-error')
          .find('.ant-form-explain')
          .should('contain', 'Please enter your Password')
  
      })

      it('Confirm Password Field >> .type() - type into a DOM element', () => {
        // https://on.cypress.io/type
        cy.get('#confirmPassword')
          .type('12345678').should('have.value', '12345678')
    
          // .type() with special character sequences
          .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
          .type('{del}{selectall}{backspace}')
    
          // .type() with key modifiers
          .type('{alt}{option}') //these are equivalent
          .type('{ctrl}{control}') //these are equivalent
          .type('{meta}{command}{cmd}') //these are equivalent
          .type('{shift}')
    
           // Verify that attr type is password
          .should('have.attr', 'type',  'password')
          
          // Verify an error if field is empty
          .closest('.has-error')
          .find('.ant-form-explain')
          .should('contain', 'Please enter your Confirm Password')

          cy.get('#password')
            .type('12345678');

          // Verify if password field and confirm are diferents
          cy.get('#confirmPassword')
            .type('1234567')
            .closest('.has-error')
            .find('.ant-form-explain')
            .should('contain', 'Two passwords that you enter is inconsistent!')
      })

    it('.submit() - submit a form', () => {
      // https://on.cypress.io/submit
      cy.get('form')
        .submit()
        .find('[type="submit"]')
        .should('be.disabled')
        // .should('contain', 'Your form has been submitted!')

        cy.get('#name')
        .type('nombre de usuario')
        cy.get('#lastName')
        .type('nombre de usuario')
        cy.get('#email')
        .type('usuario@email.com')
        cy.get('#password')
        .type('nombre de usuario')
        cy.get('#confirmPassword')
        .type('nombre de usuario')

        cy.get('form')
        .submit()

        cy.get('h1')
        .should('contain', 'Muy bien usuario@email.com será tu nombre de usuario. Ahora revisa tu correo para confirmat tu cuenta y estarás dentro!')
    })


  

  })
  