/// <reference types="Cypress" />

context('Actions', () => {
    beforeEach(() => {
      cy.visit('http://localhost:9003/#/SignIn')
    })
  
    // https://on.cypress.io/interacting-with-elements
  
   
    it('Email Field >> .type() - type into a DOM element', () => {
      // https://on.cypress.io/type
      cy.get('#email')
        .type('fake@email.com').should('have.value', 'fake@email.com')
  
        // .type() with special character sequences
        .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
        .type('{del}{selectall}{backspace}')
  
        // .type() with key modifiers
        .type('{alt}{option}') //these are equivalent
        .type('{ctrl}{control}') //these are equivalent
        .type('{meta}{command}{cmd}') //these are equivalent
        .type('{shift}')

        .should('have.attr', 'type',  'email')

        .closest('.has-error')
        .find('.ant-form-explain')
        .should('contain', 'Please enter your email')

    })

    it('Password Field >> .type() - type into a DOM element', () => {
        // https://on.cypress.io/type
        cy.get('#password')
          .type('12345678').should('have.value', '12345678')
    
          // .type() with special character sequences
          .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
          .type('{del}{selectall}{backspace}')
    
          // .type() with key modifiers
          .type('{alt}{option}') //these are equivalent
          .type('{ctrl}{control}') //these are equivalent
          .type('{meta}{command}{cmd}') //these are equivalent
          .type('{shift}')
    
          .should('have.attr', 'type',  'password')
          
          .closest('.has-error')
          .find('.ant-form-explain')
          .should('contain', 'Please enter your Password')
  
      })

     
    it('.submit() - submit a form', () => {
      // https://on.cypress.io/submit
      cy.get('form')
        .submit()
        .find('[type="submit"]')
        .should('be.disabled')
        // .should('contain', 'Your form has been submitted!')

        cy.get('#email')
        .type('usuario@email.com')
        
        cy.get('#password')
        .type('nombredeusuario')

        cy.get('form')
        .submit()

        expect(location.href).to.eq('http://localhost:9003/#/Talent/123')
    })


  

  })
  