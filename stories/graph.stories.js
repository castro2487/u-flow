/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { boolean, number, text, select } from '@storybook/addon-knobs/vue'
import i18n from 'utils/Translator'

import Bar from 'components/graphs/Bar.vue'
import Radar from 'components/graphs/Radar.vue'

const labels = [
    'happy',
    'myhappy',
    'hello',
    'maximo',
    'happy',
    'myhappy',
    'hello',
    'maximo',
    'hello',
    'maximo',
    'happy',
    'myhappy',
    'hello',
    'maximo'
]
const datasets = [
    {
        label: '',
        display: false,

        data: [65, 59, 80, 81, 56, 55, 40, 100, 49, 70, 11, 26, 35, 0]
    },
    
]

// Chart.defaults.global.legend.display = false
storiesOf('components/Graph', module)
    .add('Bar', () => ({
        components: { Bar },
        props: {
            mylabels: { default: labels },
            mydatasets: { default: datasets }
        },
        template: `<div>
        <Bar
            :datasets="mydatasets" :labels="mylabels" :width="500" :height="500"
        />
        </div>`,
        i18n
    }))
    .add('Radar', () => ({
        components: { Radar },
        props: {
            mylabels: { default: labels },
            mydatasets: { default: datasets },
            myoptions: {
                default: {
                    legend: {
                        display: false
                    },
                    scale: {
                        gridLines: {
                            circular: true,
                            backgroundColor: '#ff44ff',
                            drawBorder: false
                        },
            
                    },
          
                }
            }
        },
        template: `<div>
        <Radar 
            :datasets="mydatasets" 
            :labels="mylabels" 
            :width="100" 
            :height="200"
            :options="myoptions"
        />
        </div>`,
        i18n
    }))
