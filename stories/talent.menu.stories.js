/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { boolean, number, text, select } from '@storybook/addon-knobs/vue';
import Vue from "vue";
import Burger from 'components/NavBurger.vue';

Vue.component('menu-burger',Burger);

const nav = [
    {
      id: 0,
      text: 'Home',
      page:'/Home'
    },
    {
      id: 1,
      text: 'About',
      page:'/About'
    },
    {
      id: 2,
      text: 'Contact',
      page:'/Contact'
    }
  ];

storiesOf('components/Menu', module)
    .add('Menu right', () => {
        return (
            {
                data() {
                    return {
                        links: nav
                    }
                },
                template: `<v-app dark>
                                <menu-burger align-type="align-right">
                                    <template slot="main">
                                        <a v-for="routes in links" :key="routes.id" :href="routes.page" >
                                            {{routes.text}}
                                        </a>
                                    </template>
                                    <template slot="footer">
                                        <h2>Foooter</h2>
                                    </template>
                                </menu-burger>
                            </v-app>`
            }
        )
    })
    .add('Menu Left', () => {
        return (
            {
                data() {
                    return {
                        links: nav
                    }
                },
                template: `<v-app dark>
                                <menu-burger align-type="align-left">
                                    <template slot="main">
                                        <a v-for="routes in links" :key="routes.id" :href="routes.page" >
                                            {{routes.text}}
                                        </a>
                                    </template>
                                    <template slot="footer">
                                        <h2>Foooter</h2>
                                    </template>
                                </menu-burger>
                            </v-app>`
            }
        )
    })
    .add('Menu full', () => {
        return (
            {
                data() {
                    return {
                        links: nav
                    }
                },
                template: `<v-app dark>
                                <menu-burger align-type="full">
                                    <template slot="main">
                                        <a v-for="routes in links" :key="routes.id" :href="routes.page" >
                                            {{routes.text}}
                                        </a>
                                    </template>
                                    <template slot="footer">
                                        <h2>Foooter</h2>
                                    </template>
                                </menu-burger>
                            </v-app>`
            }
        )
    })
