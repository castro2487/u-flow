/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { boolean, number, text, select } from '@storybook/addon-knobs/vue'
import FieldChooseImage from 'components/SchemaForm/fields/FieldChooseImage'
import FieldUploadVideo from 'components/SchemaForm/fields/FieldUploadVideo'
import SchemaForm from 'components/SchemaForm/'
import skillsSchema from 'schemas/skills'
import FieldSkill from 'components/SchemaForm/fields/FieldSkill'
import skillsForm from 'config/skills.form'
import userSchema from 'schemas/personalPlayoneer'
import behaviorSchema from 'schemas/behavior'
import personalForm from 'config/personalPlayoneer.form'
import behaviorForm from 'config/behavior.form'

import i18n from 'utils/Translator'



// Vue.component('dynamic-form', DynamicForm);

const mapper = {
    chooseImage: FieldChooseImage,
    sliderSkill: FieldSkill.FieldChooseImage,
    uploadVideo: FieldUploadVideo
}

storiesOf('components/Form', module)
    .add('Login', () => ({
        components: { SchemaForm },
        data() { return {
            userForm: personalForm,
            userSchema,
            mapper
        }},
        template: '<schema-form :schema="userSchema" :formData="{}" :form="userForm" :customMapper="mapper" align-type="align-right" />',
        methods: { action: linkTo('Button', 'with some emoji') },
        i18n
    }))
    .add('Behavior', () => ({
        components: { SchemaForm },
        data() { return {
            userForm: behaviorForm,
            behaviorSchema,
            mapper,
        }},
        template: '<schema-form :schema="behaviorSchema" :formData="{behaviorPosition: []}" :form="userForm" :customMapper="mapper"  align-type="align-right" />',
        methods: { action: linkTo('Button', 'with some emoji') },
        i18n
    }))
    .add('Steps', () => ({
        components: { SchemaForm },
        data() { return {
            userForm: skillsForm,
            skillsSchema,
            mapper
        }},
        template: '<schema-form :customMapper="mapper" :schema="skillsSchema" :formData="{mainSkills:{control:8}}" :form="userForm" align-type="align-right" />',
        methods: { action: linkTo('Button', 'with some emoji') },
        i18n
    }))