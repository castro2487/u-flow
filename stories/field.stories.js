/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { boolean, number, text, select } from '@storybook/addon-knobs/vue'
import UploadVideo from 'components/UploadVideo'

import i18n from 'utils/Translator'

// Vue.component('dynamic-form', DynamicForm);

storiesOf('components/Form/fields', module).add('Uploadvideo', () => ({
    components: { UploadVideo },
    props: {
        index: { type: Number },
        posterUrl: { type: String },
        label: { default: text('button label', 'Añade un vídeo que demuestre tu habilidad') },
        value: { default: text('value', '') },
        hideButton: { default: boolean('hide button', false) }
    },
    template: `<UploadVideo 
                    @upload="upload" 
                    :label="label" 
                    :value="value" 
                    :hideButton="hideButton" 
                    :posterUrl="posterUrl" 
                />`,
    methods: { upload: action('upload') },
    i18n
}))
