/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import ItemContract from 'components/ItemContract.vue'

console.log(ItemContract)

storiesOf('components/Items', module)

    .add('story as a component', () => ({
        components: { 'item-contract': ItemContract },
        template: '<item-contract :rounded="true">rounded</item-contract>'
    }))
