/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { boolean, number, text, select } from '@storybook/addon-knobs/vue';
import Vue from "vue";
import Thumb from 'components/Thumb.vue';

Vue.component('thumb',Thumb);

storiesOf('components/Thumb', module)
    .add('Image', () => {
        return (
            {
                data() {
                    return {
                        model: {
                            id: 'img-5213654',
                            source: 'https://picsum.photos/200/300',
                            title: 'titulo de la galeria',
                            link: 'http://google.com',
                            type: 'image',
                            total: 0,
                            showEdit: true,
                            showCount: true,
                        }
                    }
                },
                methods: {
                    onDelete: e => {
                        action('Delete Item')(e);
                    }
                },
                template: '<v-app><thumb  v-bind="model" @onDeleteClicked="onDelete" /></v-app>'
            }
        )
    })
    .add('Video', () => {
        return (
            {
                data() {
                    return {
                        model: {
                            id: 'img-52136543',
                            source: 'https://picsum.photos/200/300',
                            title: 'titulo de la galeria de video',
                            link: 'http://google.com',
                            type: 'video',
                            total: 4,
                            showEdit: true,
                            showCount: true,
                        }
                    }
                },
                methods: {
                    onDelete: e => {
                        action('Delete Item')(e);
                    }
                },
                template: '<v-app><thumb  v-bind="model" @onDeleteClicked="onDelete" /></v-app>'
            }
        )
    })