/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { boolean, number, text, select } from '@storybook/addon-knobs/vue'

import InfoBehavior from 'components/infoViews/InfoBehavior'
import i18n from 'utils/Translator'

const model = {
    behaviorPosition: [
        {
            position: 'delantero',

            fieldAreaHighlight: {
                fieldZone: 1,
                attachment: { url: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4' }
            },

            fieldAreaSpendMoreTime: {
                fieldZone: 10,
                attachment: ''
            },

            fieldAreaLikeToSpendMoreTime: {
                fieldZone: 3,
                attachment: ''
            },

            fieldAreaMostReceiveTheBall: {
                fieldZone: 5,
                attachment: ''
            },

            fieldAreaMostRecoverTheBall: {
                fieldZone: 6,
                attachment: ''
            },

            fieldAreaMostHittingTheBall: {
                fieldZone: 7,
                attachment: ''
            }
        }
    ]
}

const schema = {
    title: 'behavior.position',
    type: 'behavior',
    properties: {
        key: 'behaviorPosition',

        position: {
            key: 'position',
            title: 'position.position'
        },

        questions: [
            {
                key: 'fieldAreaHighlight',
                title: 'behavior.field_area_highlight',
                video: 'attachment',
                zone: 'fieldZone'
            },

            {
                key: 'fieldAreaSpendMoreTime',
                title: 'behavior.field_area_spend_more_time',
                video: 'attachment',
                zone: 'fieldZone'
            },

            {
                key: 'fieldAreaLikeToSpendMoreTime',
                title: 'behavior.field_area_like_to_spend_more_time',
                video: 'attachment',
                zone: 'fieldZone'
            },

            {
                key: 'fieldAreaMostReceiveTheBall',
                title: 'behavior.field_area_most_receive_the_ball',
                video: 'attachment',
                zone: 'fieldZone'
            },

            {
                key: 'fieldAreaMostRecoverTheBall',
                title: 'behavior.field_area_most_recover_the_ball',
                video: 'attachment',
                zone: 'fieldZone'
            },

            {
                key: 'fieldAreaMostHittingTheBall',
                title: 'behavior.field_area_most_hitting_the_ball',
                video: 'attachment',
                zone: 'fieldZone'
            },

            {
                title: 'behavior.general',
                zone: 'all'
            }
        ]
    }
}

storiesOf('components/InfoView', module).add('Behavior', () => ({
    components: { InfoBehavior },

    props: {},

    data() {
        return {
            model,
            schema
        }
    },

    template: `<InfoBehavior 
                    :model="model"
                    :schema="schema"
                />`,
    methods: { action: linkTo('Button', 'with some emoji') },
    i18n
}))
