/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { boolean, number, text, select } from '@storybook/addon-knobs/vue'
import RadarHabilities from 'components/RadarHabilities'
import InfoView from 'components/infoViews/InfoSkills.vue'
import SkillResult from 'components/SkillResult.vue'

import i18n from 'utils/Translator'
import '../app/assets/scss/index.scss'

// Vue.component('dynamic-form', DynamicForm);

const mockResult = {
    id: '5c375cba32bcb71ac6dfcfc3',
    name: 'Pollo',
    email: 'ojesworks@gmail.com',
    createdAt: '2019-01-10T14:54:50.256Z',
    active: true,
    lastName: '...',
    dni: '123456789',
    addresses: [],
    gender: 0,
    picture: 'https://uflowbucket.s3.amazonaws.com/file-1551108636853',
    birthday: '1998-02-25T03:00:00.000Z',
    sector: [],
    work: [],
    rol: [],
    model_modified: true,
    habilidades: [],
    mainSkills: {
        shooting: 4,
        head: 4,
        strategy: 4,
        passing: 4,
        lateralPlay: 3,
        finishing: 3,
        leftLeg: 4,
        rightLeg: 0,
        videos: {
            url: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4'
        },
        control: 3
    },
    tacticSkills: { vision: 4, awareness: 5, ballMoviment: 7, videos: {} },
    athleticSkills: { speed: 8, acuity: 2, mobility: 2, bodyStrength: 4, rhythm: 4, videos: {} },
    character: {
        recovery: 0,
        physicalBravery: 0,
        mentalBravery: 0,
        energy: 0,
        leadership: 0,
        communication: 0,
        videos: {
            url: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4'
        }
    },
    hasManagerPermission: false,
    hasAdminPermission: false
}

const schema = {
    title: 'Habilidades',
    key: 'mainSkills',
    color: '#ff0000',
    properties: [
        {
            key: 'shooting',
            title: 'skills.shooting',
            description: 'texto de prueba SHOOTING'
        },
        {
            key: 'head',
            title: 'skills.head',
            description: 'texto de prueba HEAD'
        },
        {
            key: 'strategy',
            title: 'skills.strategy',
            description: 'texto de prueba STRATEGY'
        },
        {
            key: 'passing',
            title: 'skills.passing',
            description: 'texto de prueba PASSING'
        },
        {
            key: 'lateralPlay',
            title: 'skills.lateralPlay',
            description: 'texto de prueba LATERALPLAY'
        },
        {
            key: 'finishing',
            title: 'skills.finishing',
            description: 'texto de prueba FINISHING'
        },
        {
            key: 'leftLeg',
            title: 'skills.leftLeg',
            description: 'texto de prueba LEFTLEG'
        },
        {
            key: 'rightLeg',
            title: 'skills.rightLeg',
            description: 'texto de prueba RIGHTLEG'
        },
        {
            key: 'control',
            title: 'skills.control',
            description: 'texto de prueba CONTROL'
        }
    ]
}

storiesOf('components/skills', module)
    .add('Graph', () => {
        return {
            components: { RadarHabilities },
            props: {
                readonly: { default: boolean('readonly', true) },
                model: { default: mockResult }
            },
            template: `<RadarHabilities 
                            title="Habilidades"  
                            :skills="[
                                {data: model.mainSkills, legend: '#ff0000'}, 
                                {data: model.tacticSkills, legend: '#ff00ff'}, 
                                {data: model.athleticSkills, legend: '#00ff00'}]"
                            />`,
            methods: {
                onChangeHandle: action('Change')
            },
            i18n
        }
    })
    .add('InfoView', () => {
        return {
            components: { InfoView },
            props: {
                readonly: { default: boolean('readonly', true) },
                model: { default: mockResult },
                schema: { default: schema }
            },
            template:
                '<InfoView title="Habilidades" :schema="{ properties: [schema]}" :model="model"/>',
            methods: {
                onChangeHandle: action('Change')
            },
            i18n
        }
    })
    .add('SkillResult', () => {
        return {
            components: { SkillResult },
            props: {
                readonly: { default: boolean('readonly', true) },
                model: { default: mockResult },
                schema: { default: schema }
            },
            template:
                '<SkillResult title="Habilidades" :schema="schema" :model="model.mainSkills"/>',
            methods: {
                onChangeHandle: action('Change')
            },
            i18n
        }
    })
