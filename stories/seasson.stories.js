/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { boolean, number, text, select } from '@storybook/addon-knobs/vue'
import RadarHabilities from 'components/RadarHabilities'
import InfoView from 'components/infoViews/InfoSeason.vue'
import SkillResult from 'components/SkillResult.vue'

import i18n from 'utils/Translator'
import '../app/assets/scss/index.scss'

// Vue.component('dynamic-form', DynamicForm);

const mockResult = {
    id: '5c375cba32bcb71ac6dfcfc3',
    name: 'Pollo',
    email: 'ojesworks@gmail.com',
    createdAt: '2019-01-10T14:54:50.256Z',
    active: true,
    lastName: '...',
    dni: '123456789',
    addresses: [],
    gender: 0,
    picture: 'https://uflowbucket.s3.amazonaws.com/file-1551108636853',
    birthday: '1998-02-25T03:00:00.000Z',
    sector: [],
    work: [],
    rol: [],
    model_modified: true,
    habilidades: [],
    mainSkills: {
        shooting: 4,
        head: 4,
        strategy: 4,
        passing: 4,
        lateralPlay: 3,
        finishing: 3,
        leftLeg: 4,
        rightLeg: 0,
        videos: {
            url: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4'
        },
        control: 3
    },

    tacticSkills: { vision: 4, awareness: 5, ballMoviment: 7, videos: {} },

    athleticSkills: { speed: 8, acuity: 2, mobility: 2, bodyStrength: 4, rhythm: 4, videos: {} },

    character: {
        recovery: 0,
        physicalBravery: 0,
        mentalBravery: 0,
        energy: 0,
        leadership: 0,
        communication: 0,
        videos: {
            url: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4'
        }
    },

    seasons: [
        {
            year: 1979,
            semester: 1,
            month: 1,
            totalTeamMatchs: 2,
            totalPlayerMatchs: 2,
            minutesPlayed: 2,
            totalGoals: 2,
            totalKeyPasses: 2,
            totalYellowCards: 2,
            totalRedCards: 2,
        },

        {
            year: 1979,
            semester: 2,
            month: 2,
            totalTeamMatchs: 3,
            totalPlayerMatchs: 3,
            minutesPlayed: 3,
            totalGoals: 3,
            totalKeyPasses: 3,
            totalYellowCards: 3,
            totalRedCards: 3,
        },
        {
            year: 1979,
            semester: 1,
            month: 2,
            totalTeamMatchs: 1,
            totalPlayerMatchs: 1,
            minutesPlayed: 1,
            totalGoals: 1,
            totalKeyPasses: 1,
            totalYellowCards: 1,
            totalRedCards: 1,
        },

        {
            year: 1980,
            semester: 2,
            month: 1,
            totalTeamMatchs: 1,
            totalPlayerMatchs: 1,
            minutesPlayed: 1,
            totalGoals: 1,
            totalKeyPasses: 1,
            totalYellowCards: 1,
            totalRedCards: 1,
        }
    ],

    hasManagerPermission: false,
    hasAdminPermission: false
}



/*seasons = [
            [{year: 1, first: 1, experiences: []}]
        ]*/

/*
    year: {type: Number}, 
    semester: {type: Number},
    clubName: {type: String},
    clubLogo: {type: String},

    teamMatches: {type: Number},
    playerMatches: {type: Number},
    minutesPlayed: {type: Number},
    goals: {type: Number},
    keyPasses: {type: Number},
    yellowCards: {type: Number},
    redCards: {type: Number},
    ranking: {type: Number},
    trainner: {type: String},
*/

const schema = {
    title: 'sessons',
    key: 'seasons',
    properties: [
        {
            key: 'gamesPlayed',
            title: 'season.games_played'
        },
        {
            key: 'minutesPlayed',
            title: 'season.minutes_played'
        },
        {
            key: 'goles',
            title: 'season.goles'
        },
        {
            key: 'keyPasses',
            title: 'season.keyPasses'
        },
        {
            key: 'yellowCards',
            title: 'season.yellowCards'
        },
        {
            key: 'redCards',
            title: 'season.redCards'
        }
    ]
}

storiesOf('components/Season', module).add('InfoView', () => {
    return {
        components: { InfoView },
        props: {
            readonly: { default: boolean('readonly', true) },
            model: { default: mockResult },
            schema: { default: schema }
        },
        template:
            '<InfoView title="Habilidades" :schema="{ properties:[schema] }" :model="model"/>',
        methods: {
            onChangeHandle: action('Change')
        },
        i18n
    }
})
