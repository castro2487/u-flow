/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { boolean, number, text, select } from '@storybook/addon-knobs/vue'
import PositionArea from 'components/PositionArea'

import i18n from 'utils/Translator'

// Vue.component('dynamic-form', DynamicForm);

const mockResult = {
    position: '',

    fieldAreaHighlight: {
        fieldZone: 10,
        attchment: ''
    },

    fieldAreaSpendMoreTime: {
        fieldZone: 8,
        attchment: ''
    },

    fieldAreaLikeToSpendMoreTime: {
        fieldZone: 0,
        attchment: ''
    },

    fieldAreaMostReceiveTheBall: {
        fieldZone: 1,
        attchment: ''
    },

    fieldAreaMostRecoverTheBall: {
        fieldZone: 5,
        attchment: ''
    },

    fieldAreaMostHittingTheBall: {
        fieldZone: 1,
        attchment: ''
    }
}

storiesOf('components/General', module).add('PositionArea', () => {
    return {
        components: { PositionArea },
        props: {
            readonly: { default: boolean('readonly', true) },
            result: { default: mockResult}
        },
        template: '<PositionArea @change="onChangeHandle" :readonly="readonly" :result="result"/>',
        methods: {
            onChangeHandle: action('Change')
        },
        i18n
    }
})
