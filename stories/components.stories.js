/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { boolean, number, text, select } from '@storybook/addon-knobs/vue'
import Library from 'components/Library'
import MediaSource from 'components/MediaSource'
import UploadVideo from 'components/UploadVideo'
import store from 'store'

import i18n from 'utils/Translator'

// Vue.component('dynamic-form', DynamicForm);

const sourceType = {
    youtube: 'youtube',
    vimeo: 'vimeo',
    html: 'html',
    custom: 'custom'
}

storiesOf('components/ui', module)
    .add('Library', () => ({
        components: { Library },
        props: {
            index: { type: Number },
            posterUrl: { type: String },
            label: { default: text('button label', 'Añade un vídeo que demuestre tu habilidad') },
            value: { default: text('value', '') },
            hideButton: { default: boolean('hide button', false) }
        },
        template: `<Library 
                        @select="select" 
                        :title="label" 
                        :type="value" 
                    />`,
        methods: { select: action('uploaselectd') },
        i18n,
        store,
    }))
    .add('MediaSource', () => ({

        

        components: { MediaSource },

        props: {
            sourceType: { default: select('sourceType', sourceType, 'custom') },
            custom: { default: text('custom url', '') },
            type: { default: text('type', 'video') },
            height: { default: text('Height', '250') },
            width: { default: text('Width', '100%') }
        },

        template: `<MediaSource 
                        :source="source" 
                        :type="type" 
                        :height="height" 
                        :width="width" 
                    />`,
        i18n,

        computed: {
            source() {
                switch (this.sourceType) {
                    case 'youtube':
                        return 'https://www.youtube.com/watch?v=qzPsTD6Vuzg'
                    case 'vimeo':
                        return 'https://player.vimeo.com/video/56282283'
                    case 'html':
                        return 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4'
                    default:
                        return this.custom
                }
            }
        }
    }))

    .add('UploadFile', () => ({
        components: { UploadVideo },
        props: {
            index: { type: Number },
            posterUrl: { type: String },
            label: { default: text('button label', 'Añade un vídeo que demuestre tu habilidad') },
            value: { default: text('value', '') },
            hideButton: { default: boolean('hide button', false) }
        },
        template: `<UploadVideo 
                        @upload="upload" 
                        :label="label" 
                        :value="value" 
                        :hideButton="hideButton" 
                        :posterUrl="posterUrl" 
                    />`,
        methods: { upload: action('upload') },
        i18n
    }))
