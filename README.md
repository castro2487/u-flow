# Playonner

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run start
```

### Compiles and minifies for staging
```
npm run build:prepro
```

### Compiles and minifies for production
```
npm run build:prod
```

### Run your tests
```
npm run start:storybook
or
npm run start:cypress

```
## REFERENCIAS ##

*  https://vuejs.org

*  https://vuex.vuejs.org/

*  https://router.vuejs.org/

*  https://vuetifyjs.com

*  https://storybook.js.org/

*  https://www.cypress.io/

*  https://vuecomponent.github.io/ant-design-vue/

*  https://kutlugsahin.github.io/vue-smooth-dnd/

*  https://www.npmjs.com/package/vue-chartjs
