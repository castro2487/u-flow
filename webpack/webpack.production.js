const path = require('path');
const webpack = require('webpack');
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: {
    main: './app/main.js',
    vendor: ['vue', 'vuex', 'vue-router', 'vue-i18n']
  },
  output: {
    path: path.resolve(__dirname, '../public', process.env.DIR_NAME),
    filename: 'js/[name]_bundle_[hash].js',
    publicPath: './',
    
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['vue-style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              data: `
              @import "./app/assets/scss/_colors.scss";
              @import "./app/assets/scss/_variables.scss";
              @import "./app/assets/scss/_mixins.scss";
              `
            }
          }
        ]
      },
      {
        test: /\.sass$/,
        use: ['vue-style-loader', 'css-loader', 'sass-loader?indentedSyntax']
      },

      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            scss: ['vue-style-loader', 'css-loader', 'sass-loader'],
            sass: ['vue-style-loader', 'css-loader', 'sass-loader?indentedSyntax']
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: ['@babel/preset-env'],
          plugins: [require('@babel/plugin-syntax-dynamic-import')]
        }
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]',
          outputPath: (url, resourcePath, context) => {
            console.log(url, resourcePath, context)
            let dir = 'images'
            if (/\.svg/.test(url)) {
              dir = `svgs`;
            }
            return `app/assets/${dir}/${url}`;
          },
        }
      }                                                                                                                                                                                                                                                       
    ]
  },
  resolve: {
    modules: [path.resolve('./node_modules'), path.resolve('./app')],
    alias: {
      vue$: 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },

  performance: {
    hints: false
  },

  optimization: {
    splitChunks: {
      chunks: 'async',
      minSize: 30000,
      maxSize: 0,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: '~',
      name: true,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    }
  },

  plugins: [
    new CleanWebpackPlugin([`public/${process.env.DIR_NAME}`], { root: path.resolve(__dirname, '..') }),

    new CopyWebpackPlugin([{ from: 'locale', to: 'locale' }]),

    new VueLoaderPlugin(),

    new HtmlWebpackPlugin({
      template: 'app/index.html',
      allChunks: true
    }),

    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(true),
      ROOT_PATH: JSON.stringify(process.env.ROOT_PATH),
      API_URI: JSON.stringify(process.env.API_URI),
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    }),

    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ]
};
