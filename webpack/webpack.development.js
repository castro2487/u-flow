const path = require('path')
const webpack = require('webpack')
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: './app/main.js',
    output: {
        path: path.resolve(__dirname, './public'),
        filename: '[name]_bundle.js',
        chunkFilename: '[name].bundle.js',
        publicPath: '/',
        filename: 'build.js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['vue-style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            data: `
                @import "./app/assets/scss/_colors.scss";
                @import "./app/assets/scss/_variables.scss";
                @import "./app/assets/scss/_mixins.scss";
                `
                        }
                    }
                ]
            },
            {
                test: /\.sass$/,
                use: ['vue-style-loader', 'css-loader', 'sass-loader?indentedSyntax']
            },

            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
                        // the "scss" and "sass" values for the lang attribute to the right configs here.
                        // other preprocessors should work out of the box, no loader config like this necessary.
                        scss: ['vue-style-loader', 'css-loader', 'sass-loader'],
                        sass: ['vue-style-loader', 'css-loader', 'sass-loader?indentedSyntax']
                    }
                    // other vue-loader options go here
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ['@babel/preset-env'],
                    plugins: [require('@babel/plugin-syntax-dynamic-import')]
                }
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]'
                }
            }
        ]
    },

    resolve: {
        modules: [path.resolve('./node_modules'), path.resolve('./app')],
        alias: {
            vue$: 'vue/dist/vue.esm.js'
        },
        extensions: ['*', '.js', '.vue', '.json']
    },

    devServer: {
        port: 9003
    },

    performance: {
        hints: false
    },

    devtool: '#eval-source-map',

    plugins: [
        new VueLoaderPlugin(),

        new webpack.HotModuleReplacementPlugin(),

        new HtmlWebpackPlugin({
            template: 'app/index.html'
        }),

        new webpack.DefinePlugin({
            ROOT_PATH: JSON.stringify(process.env.ROOT_PATH),
            API_URI: JSON.stringify(process.env.API_URI),
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        })
    ]
}
