import ClientServices from 'utils/ClientServices'

// WORKS SERVICES
export function CreateWork(data) {
    return ClientServices.post(`works/createWork`, data);
}

/* Update put
Actualizar/Guardar trabajo = Put /works/updateWork/:id (donde :id es el id del trabajo a actualizar/guardar) 
parametros necesarios es el id del work,  opcionales son los del schema (address, date, media, sector, work, rol,idUser)
Caso de guardar exitosamente devuelve 200 con el objeto creado, caso contrario devuelve 4XX
*/
export function UpdateWork(data) {
    return ClientServices.put(`works/updateWork/${data.workId}`, data.workData)
}

// Delete delete

/* Get all
Traer lista de trabajos de un usuario = Get /works/getByUser/:id (donde :id es el id del User) . 
Devuelve todo el schema de works indexado por id de trabajo  (address, date, media, sector, work, rol,idUser)
*/
export function GetAllWorks({ id, visibility = 'All', ...data }) {
    return ClientServices.get(`works/${visibility}/${id}`, data)
}

/* 
 GET >> Obtiene todos los trabajos de un book
*/
export function GetAllWorksInBook({ id, ...data }) {
    return ClientServices.get(`portfolio/getPortfolio/${id}`, data)
}
/* 
  PUT >> Guarda el ordenamiento de un book
  @params id: identificador del usuario
  @params book: identificador del usuario
*/
export function SaveOrderInBook({ id, ...data }) {
    return ClientServices.put(`portfolio/updatePortfolio/${id}`, data)
}

/*
Traer un trabajo especifico = Get /works/getbyWork/:id (donde :id es el id del trabajo) . Devuelve todo el schema de work referente al id  (address, date, media, sector, work, rol,idUser)
*/
export function GetWorkById(data) {
    return ClientServices.get(`works/${data.workId}`, data)
}

/*
Actualizar toggle mostrar o no en trabajos  = Put /works/showInWorks/:id (donde :id es el id del trabajo) Como parametro requerido se envia showWork que es solo booleano
Actualizar toggle mostrar o no en Book  = Put  /works/showInBook/:id (donde :id es el id del trabajo) Como parametro requerido se envia showBook que es solo booleano

Para todos es requerido enviar como parametro en el body el acces_token junto a su id correspondiente que necesite.Tambien todos en caso exitoso devuelve un 2XX (200 a 299)
En caso de error, segun el tipo devuelve una variedad de 4XX y 5XX (editado) 
Ahora voy a empezar a generar de nuevo el api docs, asi nos queda documentado en un solo lugar  y mas prolijo
*/

export function ChangeWorkVisibility(data) {
    return ClientServices.put(`works/${data.visibility}/${data.workId}`, data)
}
