import * as WorkServices from './works.services'
import { StatusFetching, StatusSuccess, StatusError, DataMutation } from 'utils/StoreStatus'
import { stringify } from 'querystring'

// initial state
const state = {
    collection: [],
    current: [],
    status: { code: -1, type: '', info: {} }
}

const getters = {}

function convertToString(data) {
    var toConvertList = ['photos', 'work', 'sector', 'rol', 'manager', 'copyRight']
    const length = toConvertList.length
    for (let index = 0; index < length; index++) {
        let prop = toConvertList[index]
        data[prop] = JSON.stringify(data[prop])
    }
}

function convertToArray(data) {
    var toConvertList = ['photos', 'work', 'sector', 'rol', 'manager', 'copyRight']
    const length = toConvertList.length
    for (let index = 0; index < length; index++) {
        let prop = toConvertList[index]
        if (null != data[prop]) {
            data[prop] = JSON.parse(data[prop])
        }
    }
}

function convertArrayList(list) {
    var length = list.length
    for (var index = 0; index < length; index++) {
        convertToArray(list[index])
    }
}

const actions = {
    // Create Work
    CreateWork({ commit, state, rootState }, data) {
        data.access_token = this.getters['Auth/token'];
        data.idUser = this.getters['Auth/user'].id;
        StatusFetching(commit)
        console.log("COUNTER 1");
        return WorkServices.CreateWork(data)
            .then(function(response) {
                console.log("COUNTER 2");
                
                return StatusSuccess(commit, response)
            })
            .catch(response => {
                console.log('CreateWork >> ERROR >>', response)
                return StatusError(commit, response)
            })
    },

    // Get Work by Id
    GetWorkById({ commit, state, rootState }, data) {
        StatusFetching(commit)
        data.id = this.getters['User/me'].id
        // data.access_token = this.getters['Auth/token']
        return WorkServices.GetWorkById(data)
            .then(function(response) {
                // convertToArray(response)
                StatusSuccess(commit, {}, null, { current: response || {} })
                return response
            })
            .catch(response => {
                console.log('GetWorkById >> ERROR >>', response)
                return StatusError(commit, response)
            })
    },

    // Get Work by Id
    GetAllWorks({ commit, state }, data) {
        const params = {
            ...data,
            access_token: this.getters['Auth/token']
        }
        StatusFetching(commit)
        return WorkServices.GetAllWorks(params)
            .then(function(response) {
                return StatusSuccess(commit, {}, null, { collection: response || [] })
            })
            .catch(response => {
                console.log('GetAllWorks >> ERROR >>', response)
                return StatusError(commit, response)
            })
    },

    GetAllWorksInBook({ commit, state }, data) {
        const params = {
            ...data,
            access_token: this.getters['Auth/token']
        }
        StatusFetching(commit)
        return WorkServices.GetAllWorksInBook(params)
            .then(function(response) {
                console.log(response)
                return StatusSuccess(commit, {}, null, { collection: response.book || [] })
            })
            .catch(response => {
                console.log('GetAllWorksInBook >> ERROR >>', response)
                return StatusError(commit, response)
            })
    },

    SaveOrderInBook({ commit, state }, data) {
        const params = {
            ...data,
            access_token: this.getters['Auth/token']
        }
        StatusFetching(commit)
        return WorkServices.SaveOrderInBook(params)
            .then(function(response) {
                return StatusSuccess(commit, {}, null, { collection: response || [] })
            })
            .catch(response => {
                console.log('SaveOrderInBook >> ERROR >>', response)
                return StatusError(commit, response)
            })
    },

    ChangeWorkVisibility({ commit, state }, data) {
        const params = {
            ...data,
            access_token: this.getters['Auth/token']
        }

        StatusFetching(commit)

        const collection = state.collection.map( work => {
            const item = { ...work }
            if (item.id === data.workId) {
                const prop = data.visibility === 'showInBook' ? 'showBook' : 'showWork'
                item[prop]  = data[prop]
            }
            return item
        })

        commit('SetData', { collection })

        return WorkServices.ChangeWorkVisibility(params)
            .then(function(response) {
                return StatusSuccess(commit, {}, null)
            })
            .catch(response => {
                console.log('ChangeWorkVisibility >> ERROR >>', response)
                return StatusError(commit, response)
            })
    },

    UpdateWork({ commit, state }, data) {
        const params = {
            ...data,
            access_token: this.getters['Auth/token']
        }

        StatusFetching(commit)
        return WorkServices.UpdateWork(params)
            .then(function(response) {
                return StatusSuccess(commit, {}, null, { current: response })
            })
            .catch(response => {
                console.log('UpdateWork >> ERROR >>', response)
                return StatusError(commit, response)
            })
    }
}

// mutations
const mutations = {
    ...DataMutation
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
