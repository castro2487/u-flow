import { DataMutation } from 'utils/StoreStatus'

const state = {
    open: false,
    current: {},
    talent: {
        online: [
            {
                label: 'about you',
                to: { name: 'About' },
                class: 'title'
            },
            {
                label: 'Cerrar sessión',
                to: { name: 'LogOut' },
                class: 'small color-gray'
            }
        ],
        offline: [
            {
                label: 'home',
                to: {
                    name: 'Portada',
                },
                class: 'text-white hamburger-button'
            },
            {
                label: 'about',
                to: {
                    name: 'About',
                },
                class: 'text-white hamburger-button'
            },
            /*{
                label: 'temporadas',
                to: {
                    name: 'temporadas',
                },
                class: 'text-white hamburger-button'
            },*/
            {
                label: 'Cerrar sessión',
                to: { name: 'LogOut' },
                class: 'text-white hamburger-button'
            }
        ]
    },
    manager: {
        online: [
            {
                label: 'portfolio',
                to: { name: 'Portada' },
                class: 'text-white hamburger-button'
            },
            {
                label: 'talentos',
                to: { name: 'Contracts' },
                class: 'text-white hamburger-button'
            },
            {
                label: 'about you',
                to: { name: 'About' },
                class: 'text-white hamburger-button'
            },
            {
                label: 'facturación',
                to: { name: '' },
                class: 'text-white hamburger-button'
            },
            {
                label: 'Cerrar sessión',
                to: { name: 'LogOut' },
                class: 'text-white hamburger-button'
            }
        ],
        offline: [
            {
                label: 'about you',
                to: { name: 'About' },
                class: 'text-white hamburger-button'
            }
        ]
    }
}

const actions = {
    Open({ commit, state }, data) {
        commit('SetData', { open: true, current: state.talent.offline })
    },

    Close({ commit, state }, data) {
        commit('SetData', { open: false })
    } 
}

// mutations
const mutations = {
    ...DataMutation
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
