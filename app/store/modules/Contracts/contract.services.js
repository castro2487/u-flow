import ClientServices from 'utils/ClientServices'

// CONTRATOS

/**
 *
 * @param {userId: String, contractId: string} ContractInfo
 */
export function ConfirmContract({ userId, contractId, ...data }) {
    return ClientServices.put(`contract/${userId}/confirm/${contractId}`, {
        ...data,
    })
}

/**
 *
 * @param {userId: String, contractId: string} ContractInfo
 */
export function CreateContract({ userId, contractId }) {
    return ClientServices.put(`contract/addContract/${contractId}`)
}

/**
 *
 * @param {userId: String, contractId: string} ContractInfo
 */
export function RejectContract({ userId, contractId, ...data }) {
    return ClientServices.put(`contract/${userId}/reject/${contractId}`, {
        ...data,
    })
}

/**
 *
 * @param {userId: String, contractId: string} ContractInfo
 */
export function GetContract({ userId, contractId, ...data }) {
    return ClientServices.get(
        `contract/${userId}/getContractsByIds/${contractId}`,
        {
            ...data,
        }
    )
}
/**
 *
 * @param {userId: String, contractId: string} ContractInfo
 */
export function GetContracts({ userId, contractId, ...data }) {
    return ClientServices.get(`contract/${userId}/getContracts/`, {
        ...data,
    })
}

/**
 *
 * @param {userId: String, contractId: string} ContractInfo
 */
export function UpdateContract({ userId, contractId, ...data }) {
    return ClientServices.put(`contract/${userId}/updateContract/${contractId}`, {
        ...data,
    })
}

/**
 *
 * @param {userId: String, contractId: string} ContractInfo
 */
export function SearchContracts({ userId, query, ...data }) {
    return ClientServices.get(`contract/${userId}/search/${query}`, {
        ...data,
    })
}
