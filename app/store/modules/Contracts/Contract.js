import * as ContractServices from './contract.services'
import {
    StatusFetching,
    StatusSuccess,
    StatusError,
    DataMutation
} from 'utils/StoreStatus'

// initial state
const state = {
    request: {},
    status: { code: -1, type: '', info: {} }
}

const getters = {}

const actions = {
    RejectContract({ commit, state, dispatch }, data) {
        StatusFetching(commit, null, 0, {
            request: {
                method: 'RejectContract',
                params: data
            }
        })

        return ContractServices.RejectContract(data)
            .then(response => {
                return StatusSuccess(commit, {
                    method: 'RejectContract',
                    data: response
                })
            })
            .catch(response => {
                console.warn('RejectContract() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    ConfirmContract({ commit, state, dispatch }, data) {
        StatusFetching(commit, null, 0, {
            request: {
                method: 'ConfirmContract',
                params: data
            }
        })

        return ContractServices.ConfirmContract(data)
            .then(response => {
                return StatusSuccess(commit, {
                    method: 'ConfirmContract',
                    data: response
                })
            })
            .catch(response => {
                console.warn('ConfirmContract() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    GetContract({ commit, state, dispatch }, data) {
        StatusFetching(commit, null, 0, {
            request: {
                method: 'GetContract',
                params: data
            }
        })

        return ContractServices.GetContract(data)
            .then(response => {
                return StatusSuccess(commit, {
                    method: 'GetContract',
                    data: response
                })
            })
            .catch(response => {
                console.warn('GetContract() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    GetContracts({ commit, state, dispatch }, data) {
        StatusFetching(commit, null, 0, {
            request: {
                method: 'GetContracts',
                params: data
            }
        })

        return ContractServices.GetContracts(data)
            .then(response => {
                return StatusSuccess(commit, {
                    method: 'GetContracts',
                    data: response
                })
            })
            .catch(response => {
                console.warn('GetContracts() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    UpdateContract({ commit, state, dispatch }, data) {
        StatusFetching(commit, null, 0, {
            request: {
                method: 'UpdateContract',
                params: data
            }
        })

        return ContractServices.UpdateContract(data)
            .then(response => {
                return StatusSuccess(commit, {
                    method: 'UpdateContract',
                    data: response
                })
            })
            .catch(response => {
                console.warn('UpdateContract() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    SearchContract({ commit, state, dispatch }, data) {
        StatusFetching(commit, null, 0, {
            request: {
                method: 'SearchContract',
                params: data
            }
        })

        return ContractServices.SearchContracts(data)
            .then(response => {
                return StatusSuccess(commit, {
                    method: 'SearchContract',
                    data: response
                })
            })
            .catch(response => {
                console.warn('SearchContract() CatchError >>', response)
                return StatusError(commit, response)
            })
    }
}

// mutations
const mutations = {
    ...DataMutation
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
