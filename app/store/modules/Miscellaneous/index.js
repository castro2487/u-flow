import * as Services from './thirdparty.services'
import { uniqBy } from 'lodash/array'
import { StatusFetching, StatusSuccess, StatusError, DataMutation } from 'utils/StoreStatus'
import { range } from 'utils/helpers'

const SizeRange = (since, until, per, exception = [], extra = []) =>
    range(since, until, per)
        .filter(size => !exception.includes(size))
        .concat(extra)
        .sort()
        .map(label => ({
            label
        }))

// initial state
const state = {
    Agents: [/*{ value: 'casting@sf40agency.com', label: 'SF40 Agency' }*/],

    Beard: [
        { label: 'Sin barba' },
        { label: 'Bigote' },
        { label: 'Perilla' },
        { label: 'Patillas largas' },
        { label: 'Barba corta' },
        { label: 'Barba larga' }
    ],

    Complexion: [
        { label: 'Delgado' },
        { label: 'Muy delgado' },
        { label: 'Normal' },
        { label: 'Atlético' },
        { label: 'Musculoso' },
        { label: 'Corpulento' },
        { label: 'Curvy' }
    ],

    DrivingLicense: [
        { label: 'Ciclomotores' },
        { label: 'Motocicletas' },
        { label: 'Automóviles' },
        { label: 'Camiones sin acoplado' },
        { label: 'Tractores agrícolas' },
        { label: 'Maquinaria especial agrícola' }
    ],

    Etnia: [
        { label: 'Asiática' },
        { label: 'Árabe' },
        { label: 'Negra' },
        { label: 'Africana' },
        { label: 'Afroamericana' },
        { label: 'Caucásica' },
        { label: 'India' },
        { label: 'Latina' }
    ],

    EyesColor: [
        { label: 'azul' },
        { label: 'gris' },
        { label: 'marrón' },
        { label: 'miel' },
        { label: 'negro' },
        { label: 'verde' }
    ],

    ExpertiseLevel: [
        { label: 'Básico' },
        { label: 'Intermedio' },
        { label: 'Avanzado' },
        { label: 'Nativo' }
    ],

    Gender: [
        { value: 0, label: 'Hombre' },
        { value: 1, label: 'Mujer' },
        { value: 2, label: 'Neutro' }
    ],

    HairColor: [
        { label: 'Cano' },
        { label: 'Castaño claro' },
        { label: 'Castaño oscuro' },
        { label: 'Marron chocolate' },
        { label: 'Rubio claro' },
        { label: 'Rubio oscuro' },
        { label: 'Gris' },
        { label: 'Negro' },
        { label: 'Pelirrojo' },
        { label: 'Fantasía' },
        { label: 'Otro' }
    ],

    HairStyle: [
        { label: 'liso' },
        { label: 'ondulado' },
        { label: 'rizado' },
        { label: 'rasta' },
        { label: 'afro' },
        { label: 'trenzas' },
        { label: 'otro' }
    ],

    HairLong: [
        { label: 'Calvo' },
        { label: 'Rapado' },
        { label: 'Corto' },
        { label: 'Media melena' },
        { label: 'Largo' },
        { label: 'Muy largo' },
        { label: 'Otra' }
    ],
    Language: [
        { label: 'Chino' },
        { label: 'Inglés' },
        { label: 'Euskera' },
        { label: 'Castellano' },
        { label: 'Catalán' },
        { label: 'Gallego' },
        { label: 'Portugués' },
        { label: 'Francés' },
        { label: 'Ruso' },
        { label: 'Arabe' }
    ],

    Positions: [
        { label: 'Delantero' },
        { label: 'Centrocampista' },
        { label: 'Banda derecha' },
        { label: 'Banda izquierda' },
        { label: 'Defensa' },
        { label: 'Portero' }
    ],

    SizeShoes: {
        0: {
            EUR: [...SizeRange(38, 53, 0.5)],
            UK: [...SizeRange(5, 18, 0.5)],
            US: [...SizeRange(6, 18, 0.5)]
        },
        1: {
            EUR: [...SizeRange(34, 42, 0.5)],
            UK: [...SizeRange(1, 9, 0.5)],
            US: [...SizeRange(4, 14, 0.5)]
        }
    },

    SizeTop: {
        0: {
            EUR: [...SizeRange(46, 56, 2)],
            UK: [...SizeRange(36, 46, 2)],
            US: [...SizeRange(36, 46, 2)],
            MEX: [...SizeRange(36, 46, 2)],
            IT: [...SizeRange(50, 60, 2)]
        },
        1: {
            EUR: [...SizeRange(32, 48, 2)],
            UK: [...SizeRange(4, 20, 2)],
            US: [...SizeRange(1, 16, 2)],
            MEX: [...SizeRange(1, 15, 2, [], [0])],
            IT: [...SizeRange(36, 52, 2)]
        }
    },

    SizeUnder: {
        0: {
            EUR: [...SizeRange(36, 48, 2)],
            UK: [...SizeRange(26, 38, 2)],
            US: [...SizeRange(26, 38, 2)],
            MEX: [...SizeRange(26, 38, 2)],
            IT: [...SizeRange(40, 52, 2)]
        },
        1: {
            EUR: [...SizeRange(32, 48, 2)],
            UK: [...SizeRange(4, 20, 2)],
            US: [...SizeRange(1, 16, 2)],
            MEX: [...SizeRange(1, 15, 2, [], [0])],
            IT: [...SizeRange(36, 52, 2)]
        }
    },

    Skin: [
        { label: 'Muy claro' },
        { label: 'Claro' },
        { label: 'Medio' },
        { label: 'Bronceado' },
        { label: 'Oscuro' },
        { label: 'Muy oscuro' },
        { label: 'Negro' },
        { label: 'Amarillo' }
    ],

    SpecialHabilities: [
        { label: 'Paracaidismo' },
        { label: 'Parkour' },
        { label: 'Saltos de altura' },
        { label: 'Conducción extrema' },
        { label: 'Artes marciales' },
        { label: 'Manejo de armas' },
        { label: 'Acrobacias' }
    ],

    Sports: [
        {
            label: 'Aerobic'
        },
        { label: 'Aikido' },
        { label: 'Atletismo' },
        { label: 'Automovilismo' },
        { label: 'Badminton' },
        { label: 'Baseball' },
        { label: 'Basket' },
        { label: 'Bodyboard' },
        { label: 'Boxeo' },
        { label: 'Buceo' },
        { label: 'Capoeira' },
        { label: 'Ciclismo' },
        { label: 'Culturismo' },
        { label: 'Fútbol' },
        { label: 'Hockey sobre hielo' },
        { label: 'Hockey sobre hierba' },
        { label: 'Hockey sobre patines' },
        { label: 'Judo' },
        { label: 'Karate' },
        { label: 'Kite surf' },
        { label: 'Kick boxing' },
        { label: 'Kung fu' },
        { label: 'Montañismo' },
        { label: 'Motociclismo' },
        { label: 'Padel' },
        { label: 'Patinaje artístico' },
        { label: 'Patinaje sobre hielo' },
        { label: 'Ping pong' },
        { label: 'PiragÜismo' },
        { label: 'Rugby' },
        { label: 'Skate' },
        { label: 'Ski' },
        { label: 'Snow' },
        { label: 'Surf' },
        { label: 'Squash' },
        { label: 'Taekwondo' },
        { label: 'Tai chi' },
        { label: 'Tenis' },
        { label: 'Tiro con arco' },
        { label: 'Triatlón' },
        { label: 'Vela' },
        { label: 'Voleibol' },
        { label: 'Voley playa' },
        { label: 'Waterpolo' },
        { label: 'Windsurf' },
        { label: 'Yoga' },
        { label: 'Zumba' }
    ],

    Studies: [
        { label: 'Primaria' },
        { label: 'Secondaria' },
        { label: 'Formación Profesional' },
        { label: 'Grado Superior' },
        { label: 'Grado universitario' },
        { label: 'Máster' },
        { label: 'Doctorado' }
    ],

    YesorNot: [{ value: 'true', label: 'Si' }, { value: 'false', label: 'No' }],

    countries: [],
    provincies: [],
    cities: [],
    areaCodes: [],
    Pasarela: [],
    Roles: {
        Others: [{ label: 'Otros' }],
        Reparto: [
            { label: 'Protagonista' },
            { label: 'Protagonista compartido' },
            { label: 'Secundario' },
            { label: 'Reparto Pequeñas partes' },
            { label: 'Figuración' },
            { label: 'Figuración especial' },
            { label: 'Especialista' }
        ],
        Editorial: [{ label: 'individual' }, { label: 'derechos compartidos' }]
    },
    Works: {
        0: { label: 'Pasarela', roles: 'Others' },
        1: { label: 'Publicidad', roles: 'Reparto' },
        2: { label: 'Editorial', roles: 'Editorial' },
        3: { label: 'Ficción', roles: 'Reparto' },
        4: { label: 'Congreso', roles: 'Others' },
        5: { label: 'Imagen', roles: 'Others' },
        6: { label: 'Producto', roles: 'Others' }
    },
    sector: [
        { label: 'Modelo', works: [0, 1, 2] },
        { label: 'Actor', works: [3, 1] },
        { label: 'Azafata', works: [4, 5, 6] }
    ],
    boolean: [{ value: 'true', label: 'Si' }, { value: 'false', label: 'No' }],
    status: { code: -1, type: '', info: {} }
}

const getters = {
    gender: ({ Gender }) => {
        return (value, defaultLabel = 'Unknow') =>
            (Gender.filter(option => option.value === value)[0] || { label: defaultLabel }).label
    },

    YesOrNot: ({ boolean }) => {
        return (value, defaultLabel = 'Unknow') =>
            (boolean.filter(option => option.value === value)[0] || { label: defaultLabel }).label
    }
}

const mapAreaCode = data => {
    const mapped = data.reduce((mapped, { callingCodes }) => {
        callingCodes.forEach(code => mapped.push({ value: code, label: `+${code}` }))
        return mapped
    }, [])
    return uniqBy(mapped, 'value').sort((a, b) => {
        return +a.value > +b.value ? 1 : -1
    })
}

const actions = {
    // Update user's profile data
    GetCountries({ commit, state }, data) {
        StatusFetching(commit)
        return Services.GetCountries()
            .then(response => {
                return StatusSuccess(commit, {}, null, {
                    countries: response,
                    areaCodes: mapAreaCode(response)
                })
            })
            .catch(response => {
                console.warn('GetCountries() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    GetProvinces({ commit, state }, data) {
        StatusFetching(commit)
        return Services.GetProvinces(data)
            .then(response => {
                return StatusSuccess(commit, {}, null, { provinces: response })
            })
            .catch(response => {
                console.warn('GetProvinces() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    GetCities({ commit, state }, country, province) {
        StatusFetching(commit)
        return Services.GetCities(country, province)
            .then(response => {
                return StatusSuccess(commit, {}, null, { cities: response })
            })
            .catch(response => {
                console.warn('GetCities() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    GetAreaCode({ commit, state }, country, province) {
        StatusFetching(commit)
        return Services.GetAreaCode()
            .then(response => {
                return StatusSuccess(commit, {}, null, {
                    areaCodes: mapAreaCode(response)
                })
            })
            .catch(response => {
                console.warn('GetAreaCode() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    UploadFile({ commit, rootState }, file) {
        StatusFetching(commit)
        const { user, token } = rootState.Auth.status.info
        return Services.UploadFile({ userId: user.id, access_token: token }, file)
            .then(response => {
                StatusSuccess(commit)
                return response
            })
            .catch(response => {
                console.warn('UploadFile() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    UploadFileWork({ commit, rootState }, data) {
        StatusFetching(commit)
        const file = data.file
        const fileType = data.fileType
        const { user, token } = rootState.Auth.status.info
        return Services.UploadFile(
            { userId: user.id, access_token: token, type: fileType, id: 8888 },
            file
        )
            .then(response => {
                StatusSuccess(commit)
                return response
            })
            .catch(response => {
                console.warn('UploadFileWork() CatchError >>', response)
                return StatusError(commit, response)
            })
    }
}

// mutations
const mutations = {
    ...DataMutation
}

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
}
