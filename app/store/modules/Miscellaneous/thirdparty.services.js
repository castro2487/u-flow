import ClientServices from 'utils/ClientServices';
import {BATUTA_KEY} from 'config/_constants';

export function GetCountries() {
    return ClientServices.get(`miscs/GetCountries`);
}

export function GetProvinces(countryCode) {
    return ClientServices.get(`miscs/GetProvinces/${countryCode}`);
}

export function GetCities(countryCode, region) {
    return ClientServices.get(`miscs/GetCities/${countryCode}/${region}`);
}

export function GetAreaCode() {
    return ClientServices.get(`miscs/GetAreaCode`);
}

export function UploadFile(userData, file) {
    return ClientServices.upload(`fileUpload`, 
        {...userData, file},
    );
}