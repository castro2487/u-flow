import ClientServices from 'utils/ClientServices'

// CONTRATOS

/**
 *
 * @param {userId: String, access_token: string} ContractInfo
 */
export function Add({ userId, ...params }) {
    const method = params.url ? 'post' : 'upload'
    return ClientServices[method](`video/add/${userId}`, params)
}

/**
 *
 * @param {userId: String, access_token: string} ContractInfo
 */
export function Get({ userId, ...params }) {
    return ClientServices.get(`video/get/${userId}`, params)
}

/**
 *
 * @param {userId: String, ItemId: string} ContractInfo
 */
export function Delete({ userId, itemId, ...params }) {

    return ClientServices.delete(`video/delete/${userId}/${itemId}`, params)
}
