import * as Services from './Library.services'
import { StatusFetching, StatusSuccess, StatusError, DataMutation } from 'utils/StoreStatus'

const state = {
    datasource: [],
    status: { code: -1, type: '', info: { params: {} } }
}

const actions = {
    Add({ commit, state }, { file, url }) {
        const params = {
            userId: this.getters['Auth/user'].id,
            access_token: this.getters['Auth/token'],
            file,
            url
        }

        StatusFetching(commit, { ...state.status.info, params })

        return Services.Add(params)

            .then(response => {
                return StatusSuccess(commit, { ...state.status.info, response }, null, {
                    datasource: [...state.datasource, response]
                })
            })

            .catch(response => {
                console.warn('Library.Get() CatchError >>', response)
                return StatusError(commit, { ...state.status.info, error: response })
            })
    },

    Get({ commit, state }) {
        try {
            const params = {
                userId: this.getters['Auth/user'].id,
                access_token: this.getters['Auth/token']
            }

            if (state.status.info.params.userId !== params.userId) {
                StatusFetching(commit, { ...state.status.info, params })

                return Services.Get(params)

                    .then(response => {
                        return StatusSuccess(commit, state.status.info, null, {
                            datasource: response
                        })
                    })

                    .catch(response => {
                        console.warn('Library.Get() CatchError >>', response)
                        return StatusError(commit, { ...state.status.info, error: response })
                    })
            }
        } catch (error) {}
    },

    Delete({ commit, state }, itemId) {
        try {
            const params = {
                itemId,
                userId: this.getters['Auth/user'].id,
                access_token: this.getters['Auth/token']
            }

            StatusFetching(commit, { ...state.status.info, params })

            const datasource = state.datasource.filter(item => item.id !== itemId)


            return Services.Delete(params)

                .then(response => {
                    return StatusSuccess(commit, state.status.info, null, { datasource })
                })

                .catch(response => {
                    console.warn('Library.Get() CatchError >>', response)
                    return StatusError(commit, { ...state.status.info, error: response })
                })
        } catch (error) {}
    }
}

// mutations
const mutations = {
    ...DataMutation
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
