import { StatusFetching, StatusSuccess, StatusError, DataMutation } from 'utils/StoreStatus'
import { LoadLocale, LoadSectionLocale } from 'utils/Translator'

// initial state
const state = {
    lang: ''
}

const actions = {
    // Set local languae
    SetLang({ commit, state }, lang) {
        StatusFetching(commit)
        return LoadLocale(lang)
            .then(response => {
                return StatusSuccess(commit, { method: 'SetLang', data: response }, 200, { lang })
            })
            .catch(response => {
                console.warn('SetLang() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    // Set section locale
    SetSectionLocale({ commit, state }, section) {
        StatusFetching(commit)
        return LoadSectionLocale(section)
            .then(response => {
                return StatusSuccess(commit, { section })
            })
            .catch(response => {
                console.warn('SetSecionLocale() CatchError >>', response)
                return StatusError(commit, response)
            })
    }
}

// mutations
const mutations = {
    ...DataMutation
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
