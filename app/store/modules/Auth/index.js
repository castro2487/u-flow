// 5c409a001f0f632077fb23d9 // Gonzalo
// 5c4037741f0f632077fb23d4 // Kashi
//http://localhost:9003/#/talent/5c4037741f0f632077fb23d4/work/5c497ea13d31701fd4d5f1f8

import { HOST } from 'config/_constants'
import * as AuthServices from './auth.services'
import { StatusFetching, StatusSuccess, StatusError, DataMutation } from 'utils/StoreStatus'

const SESSION = 'Playoneer'

// initial state
const defaultState = {
    email: { value: '' },
    emailConfirmed: { value: 'false' },
    password: { value: '' },
    confirmPassword: { value: '' },

    //REMOVER CUANDO SE ARREGLE EL BACKEND
    status: {
        code: -1,
        type: '',
        info: {}
    }
    /* status: {
        code: 200,
        type: '',
        info: {
            token:
                'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjNDA0ZTZiOTkyZTFhMjkxYzdlOGUzZCIsImlhdCI6MTU0ODMyNjU1OH0.ZUfVQPgdfddFUR-DPvFOJ0ChOAmqFLk33FGkfVRsGM4',
            user: { id: '5c4037741f0f632077fb23d4', model_modified: true }
        }
     }*/
}

const getters = {
    isAuthenticated: ({ status }, getters, rootState) => {
        return status.code === 200 && !!status.info.token
    },

    canEdit: ({ status }, getters, rootState) => {
        return getters.isAuthenticated
    },

    hasEmail: ({ email }) => {
        return email.value.length > 5
    },

    isCurrentUser: ({ status }) => {
        const user = status.info.user || { id: null }
        const userId = user.id || user._id
        return id => id === userId
    },

    user: ({ status }) => {
        return status.info.user
    },

    token: ({ status }) => {
        return status.info.token
    },

    talentToLink: ({ talentId }) => {
        return talentId
    }
}

const AuthUser = (dispatch, response = {}) => {
    DataPersist(response)
}

const DataPersist = ({ token, user: { id, role, talents } }) => {
    RemoveDataPresisted()
    sessionStorage.setItem(
        SESSION,
        JSON.stringify({
            token,
            user: {
                id,
                role,
                talents,
            }
        })
    )
}

const RemoveDataPresisted = () => {
    sessionStorage.removeItem(SESSION)
}

const actions = {
    // Store data
    SetData({ commit, state }, data) {
        commit('SetData', data)
    },

    // Clean password
    ResetPassword({ commit, state }, data) {
        commit('SetData', { password: { value: '' } })
    },

    // Clean password
    CleanData({ commit, state }, data) {
        commit('SetData', defaultState)
    },

    LogOut({ commit, dispatch }) {
        RemoveDataPresisted()

        const user = {}
        dispatch('User/SetAuth', user, { root: true })
        StatusSuccess(commit, {
            code: -1,
            type: '',
            info: {}
        })

        return false
    },

    AuthenticateUser({ commit, dispatch, getters }, user) {
        dispatch('AutoLogged')
        if (getters.isCurrentUser(user)) {
            StatusSuccess(commit, {
                user
            })
        }
    },

    AutoLogged({ commit, dispatch }) {
        try {
            const response = JSON.parse(sessionStorage.getItem(SESSION))
            if (response.user && response.token) {
                AuthUser(dispatch, response)
                StatusSuccess(commit, response)
                return true
            }
        } catch (error) {}

        return false
    },

    GetUserFaild({ commit, dispatch, getters }, id) {
        try {
            if (getters.isCurrentUser(id)) {
                RemoveDataPresisted()
            }
        } catch (error) {}

        return false
    },

    // Log user with token confirmation
    SignInWithToken({ commit, state, dispatch }, token) {
        StatusFetching(commit)
        return AuthServices.Verifications(token)
            .then(response => {
                // AuthUser(dispatch, response.user)
                return StatusSuccess(commit, response)
            })
            .catch(response => {
                console.warn('SignInWithToken() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    // Get validation Email
    VerifyUserMail({ commit, state }, data) {
        StatusFetching(commit)
        return AuthServices.VerifyUserMail(state.email.value)
            .then(response => {
                return StatusSuccess(commit, response)
            })
            .catch(response => {
                console.warn('VerifyUserMail() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    // Register user with data stored
    SignUp({ commit, state, dispatch }, data) {
        const role = state.status.info.role
        StatusFetching(commit)
        return AuthServices.SignUp(state.email.value, state.password.value, role)
            .then(response => {
                //AuthUser(dispatch, response)
                return StatusSuccess(commit, response)
            })
            .catch(response => {
                console.warn('SignUp() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    // Log user with data stored
    SignIn({ commit, state, dispatch }, data) {
        RemoveDataPresisted()
        const role = state.status.info.role
        StatusFetching(commit)
        return AuthServices.SignIn(state.email.value, state.password.value, role)
            .then(response => {
                // console.log('sucess', response)
                AuthUser(dispatch, response)
                return StatusSuccess(commit, response)
            })
            .catch(response => {
                console.warn('SignIn() CatchError >>', response)
                sessionStorage.removeItem(SESSION)
                return StatusError(commit, response)
            })
    },

    // Log user with data stored
    Forgot({ commit, state, dispatch }) {
        StatusFetching(commit)
        return AuthServices.Forgot({
            email: state.email.value
        })
            .then(response => {
                return StatusSuccess(commit, response)
            })
            .catch(response => {
                console.warn('Forgot() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    // Log user with data stored
    RecoverPassword({ commit, state, dispatch }) {
        const data = {
            email: state.status.info.user.email,
            password: state.password.value,
            token: state.status.info.token
        }
        StatusFetching(commit)
        return AuthServices.RecoverPassword(data)
            .then(response => {
                // console.log('sucess', response)
                AuthUser(dispatch, response)
                return StatusSuccess(commit, response)
            })
            .catch(response => {
                console.warn('RecoverPassword() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    VerifyToken({ commit, state, dispatch }, token) {
        StatusFetching(commit)
        return AuthServices.VerifyForgotToken(token)
            .then(response => {
                // console.log('sucess', response)
                return StatusSuccess(commit, response)
            })
            .catch(response => {
                console.warn('RecoverPassword() CatchError >>', response)
                return StatusError(commit, response)
            })
    }
}

// mutations
const mutations = {
    ...DataMutation
}

export default {
    namespaced: true,
    state: { ...defaultState },
    getters,
    actions,
    mutations
}
