import { MASTER_KEY } from 'config/_constants'
import ClientServices from 'utils/ClientServices'
//

export function SignInWithToken(token, email) {
    return ClientServices.post('auth/SignIn', {
        email,
        token
    })
}

export function GetEmailWithToken(token, email) {
    return ClientServices.post('auth/SignIn', {
        email,
        token
    })
}

export function SignUp(email, password, role) {
    return ClientServices.post('users', {
        email,
        password,
        access_token: MASTER_KEY,
        role
    })
}

export function SignIn(email, password, role) {
    return ClientServices.auth('auth', {
        email,
        password,
        role
    })
}

export function Verifications(id) {
    return ClientServices.get(`verifications/${id}`)
}

export function VerifyUserMail(email) {
    return ClientServices.get(`users/verifyMail/${email}`)
}

export function Forgot(data) {
    return ClientServices.post(`password-resets`, data, true)
}

export function VerifyForgotToken(token) {
    return ClientServices.get(`password-resets/${token}`)
}

export function RecoverPassword({ token, password }) {
    return ClientServices.put(`password-resets/${token}`, { password })
}

/*

Store -> Call Services -> BACKEND -> Servicio -> Store

Vuex -> StateManager - storage 

this.$store Es un singleton!

import Store from 'store' // Cuando no es un componente, es el mismo singleton

Store.dispatch('') // Genera una Accion que puede ser sync || ascyn
// Por lo que una accion te va a poder facilitar hacer un llamado a un services y luyego
// grabarlo en el store o simplemente pedidos que guarda.

Store.dispatch('Auth/SignInWithToken', parametros)
//Store.dispatch('Nombre_del_modulo/nombre_de_la_accion')

Store.commit // El que inicia el guardado de la informacion. El controller
Store.commit(Action, Data)

Store.getters['Auth/isAuthenticated']
Store.getters['Auth/isCurrentUser'](userId)

Store.state.Auth.status.code


*/
