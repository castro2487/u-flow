import * as UserServices from './user.services'
import { StatusFetching, StatusSuccess, StatusError, DataMutation } from 'utils/StoreStatus'

// initial state
const state = {
    me: {},
    status: { code: -1, type: '', info: {} }
}

const getters = {
    hasProfileCreated: state => {
        const required = ['name']
        const hasProfile = required.every(key => !!((state.me || {})[key] || '').length)
        return hasProfile
    },

    yearOld: ({ me }, getters, rootState, rootGetters) => {
        const birthday = new Date(me.birth)
        const ageDifMs = Date.now() - birthday.getTime()
        const ageDate = new Date(ageDifMs)
        const age = Math.abs(ageDate.getUTCFullYear() - 1969)
        return `${isNaN(birthday) ? '' : age} años`
    },

    me: ({ me }) => {
        return me
    }
}

const actions = {
    // Store data
    SetAuth({ commit, state }, data) {
        commit('SetData', { me: data })
    },

    // Update user's profile data
    UpdateProfile({ commit, state }, data) {
        StatusFetching(commit)
        return UserServices.UpdateUser(state.me.id, {
            ...data,
            access_token: this.getters['Auth/token']
        })
            .then(response => {
                return StatusSuccess(commit, {}, null, { me: response })
            })
            .catch(response => {
                console.warn('UpdateProfile() CatchError >>', response)
                return StatusError(commit, response)
            })
    },

    // Update user's profile data
    GetUser({ commit, state, dispatch }, id) {
        StatusFetching(commit)

        return UserServices.RetrieveCurrentUser(id)
            .then(response => {
                dispatch('Auth/AuthenticateUser', response, { root: true })
                return StatusSuccess(commit, {}, null, { me: response })
            })
            .catch(response => {
                dispatch('Auth/GetUserFaild', id, { root: true })
                console.warn('GetUser() CatchError >>', response)
                return StatusError(commit, response)
            })
    }
}

// mutations
const mutations = {
    ...DataMutation
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
