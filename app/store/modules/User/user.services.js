import ClientServices from 'utils/ClientServices'

export function DeleteUser(id) {
    return ClientServices.delete(`users/${id}`)
}

export function RetrieveCurrentUser(id) {
    return ClientServices.get(`users/${id}`)
}

export function UpdatePassword(userId, password) {
    return ClientServices.put(`users/${userId}/password`, { password })
}

export function UpdateUser(userId, data) {
    return ClientServices.put(`users/updateProfile/${userId}`, data)
}

// CONTRATOS

export function ConfirmContract({ userId, contractId }) {
    return ClientServices.put(`contract/confirm/`, {
        ...data,
        idTalent: userId,
        idManager: contractId
    })
}

export function CreateContract({ userId, contractId }) {
    return ClientServices.put(`contract/addContract/${contractId}`)
}

export function RejectContract({ userId, contractId }) {
    return ClientServices.delete(`contract/reject`, {
        ...data,
        idTalent: userId,
        idManager: contractId
    })
}

export function GetContract({ userId, contractId }) {
    return ClientServices.get(
        `contract/${userId}/getContractsByIds/${contractId}`,
        {
            ...data,
            idTalent: userId,
            idManager: contractId
        }
    )
}

export function UpdateContract({ userId, contractId, ...data }) {
    return ClientServices.get(`contract/updateContract/`, {
        ...data,
        idTalent: userId,
        idManager: contractId
    })
}
