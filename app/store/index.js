import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from '../plugins/Logger';
import User from './modules/User';
import Auth from './modules/Auth';
import Works from './modules/Works';
import Library from './modules/Library';
import Locale from './modules/Locale';
import Contract from './modules/Contracts/Contract';
import Miscs from './modules/Miscellaneous';
import Navigation from './modules/Navigation';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    Auth,
    Contract,
    Library,
    Locale,
    Miscs,
    Navigation,
    User,
    Works,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
});
