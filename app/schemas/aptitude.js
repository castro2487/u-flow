export default {
    type: 'object',
    title: 'Aptitudes',
    properties: {
        languages: {
            type: 'array',
            title: 'Aptitudes',
            items: {
                type: 'object',
                properties: {
                    language: {
                        title: 'Idioma',
                        type: 'string'
                    },

                    level: {
                        title: 'Nivel',
                        type: 'string'
                    }
                }
            }
        },

        drivingLicense: {
            type: 'array',
            title: 'Carnet de conducir'
        },

        studies: {
            type: 'array',
            title: 'Estudios'
        },

        othersStudies: {
            type: 'string',
            title: 'Otras titulaciones o estudios'
        },

        specialHabilities: {
            type: 'array',
            title: 'Habilidades especiales'
        },

        sports: {
            type: 'array',
            title: 'Deportes'
        }
    },

    required: []
};
