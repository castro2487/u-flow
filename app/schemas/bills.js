export default {
    type: 'object',
    title: 'Facturación',
    properties: {
        bill: {
            type: 'object',
            title: 'Facturación',
            properties: {
                account: {
                    type: 'string',
                    title: 'Número de cuenta'
                },

                payPal: {
                    type: 'string',
                    title: 'Paypal'
                },

                ss: {
                    type: 'string',
                    title: 'S.S.'
                }
            }
        }
    },

    required: []
};
