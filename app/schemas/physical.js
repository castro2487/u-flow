export default {
    type: 'object',
    title: 'características físicas',
    properties: {
        etnia: {
            type: 'string',
            title: 'Etnia'
        },

        height: {
            type: 'string',
            title: 'Altura'
        },

        weight: {
            type: 'string',
            title: 'Peso'
        },

        complexion: {
            type: 'string',
            title: 'Complexión'
        },

        waistMeasurement: {
            type: 'string',
            title: 'Medida de cintura'
        },

        hipMeasurement: {
            type: 'string',
            title: 'Medida de cadera'
        },

        chestMeasurement: {
            type: 'string',
            title: 'Medida de pecho'
        },

        eyes: {
            type: 'string',
            title: 'Color de ojos'
        },

        hairColor: {
            type: 'string',
            title: 'Color de pelo'
        },

        hairStyle: {
            type: 'string',
            title: 'Estilos de pelo'
        },

        hairLong: {
            type: 'string',
            title: 'Longitud de pelo'
        },

        beard: {
            type: 'string',
            title: 'Barba'
        },

        sizeShirt: {
            type: 'string',
            title: 'Talla de camiseta'
        },

        sizeJacket: {
            type: 'string',
            title: 'Talla de chaqueta'
        },

        sizeShoes: {
            type: 'string',
            title: 'Talla de calzado'
        },

        piercings: {
            type: 'string',
            title: 'Piercings',
            default: 'false'
        },

        tattoos: {
            type: 'string',
            title: 'Tattoos',
            default: 'false'
        },

        peculiarities: {
            type: 'string',
            title: 'Peculiaridades'
        }
    },

    required: []
}
