export default {
    type: 'object',
    title: 'Información basica',
    properties: {

      name: {
        type: 'string',
        maxLength: 120,
        minLength: 2,
        title: 'Nombre',
      },

      lastName: {
        type: 'string',
        maxLength: 120,
        minLength: 2,
        title: 'Surname',
      },

      nickname: {
        type: 'string',
        maxLength: 120,
        title: 'Nickname',
      },

      idnumber: {
        type: 'string',
        minLength: 6,
        maxLength: 18,
        title: 'idnumber',
        validationMessage: 'Campo inválido >> (000000R)'
      },

      weight: {
        type: 'string',
        maxLength: 220,
        title: 'Weight',
      },

      height: {
        type: 'string',
        maxLength: 220,
        title: 'Height',
      },

      nationality: {
        type: 'string',
        title: 'Nationality',
      },

      country: {
        type: 'string',
        title: 'Country',
      },

      city: {
        type: 'string',
        title: 'City',
      },

      sport: {
        type: 'string',
        title: 'Sport',
      },

      picture: {
        type: 'string',
        title: 'Foto',
      },

      gender: {
        type: 'number',
        default: 0,
        title: 'Genero',
      },

      birthday: {
        type: 'string',
        title: 'Fecha de nacimiento',
      },

        representante: {
            type: 'array',
            title: 'Agencia/Represnetante',
            required: ['name', 'lastName', 'dni', 'email'],
            properties: {
                name: {
                    type: 'string',
                    title: 'Email'
                },

                lastName: {
                    type: 'string',
                    title: 'Regiones / Paises'
                },

                dni: {
                    type: 'string',
                    title: '% revenue share'
                }, 
            }
        },

        tutor: {
            type: 'object',
            title: 'Datos del tutor',
            required: ['name', 'lastName', 'dni', 'email'],
            properties: {
                name: {
                    type: 'string',
                    title: 'Nombre'
                },

                lastName: {
                    type: 'string',
                    title: 'Apellido'
                },

                dni: {
                    type: 'string',
                    title: 'DNI'
                },

                email: {
                    type: 'string',
                    title: 'email'
                },

                phone: {
                    type: 'string',
                    title: 'Teléfono'
                }
            }
        }
    },
  
    required: ['name', 'lastName' ]
};
