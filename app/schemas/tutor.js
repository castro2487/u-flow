export default {
    type: 'object',
    title: 'Datos del tutor',
    required: ['name', 'lastName', 'dni', 'email'],
    properties: {
        name: {
            type: 'string',
            title: 'Nombre'
        },

        lastName: {
            type: 'string',
            title: 'Apellido'
        },

        dni: {
            type: 'string',
            title: 'DNI'
        },

        email: {
            type: 'string',
            title: 'email'
        },

        phone: {
            type: 'string',
            title: 'Teléfono'
        },

    }
}
