import tutor from './tutor'

export default {
    title: 'Direcciones',
    type: 'object',
    properties: {
        country: {
            type: 'string',
            title: 'País'
        },

        address: {
            type: 'string',
            title: 'Dirección principal'
        },

        code: {
            type: 'string',
            title: 'Código Postal'
        },

        city: {
            type: 'string',
            title: 'Ciudad'
        },

        province: {
            type: 'string',
            title: 'Provincia'
        }
    },
    required: ['country', 'address', 'province']
}
