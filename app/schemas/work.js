export default {
    type: 'object',
    title: 'Información básica',
    properties: {

      title: {
        type: 'string',
        title: 'Título del trabajo',
        
      },

      description: {
        type: 'string',
        title: 'Descripción',
      },

      sector: {
        type: 'array',
        title: 'Sector',
        minItems: 1,
      },

      work: {
        type: 'array',
        title: 'Tipos de trabajo',
        minItems: 1,
      },

      rol: {
        type: 'array',
        minItems: 1,
        title: 'Rol',
      },

      brand: {
        type: 'string',
        title: 'Marca (cliente)',
      },

      country: {
        type: 'string',
        title: 'País',
      },

      city: {
        type: 'string',
        title: 'Ciudad',
      },

      date: {
        type: 'string',
        title: 'Fecha',
      },

      cache: {
        type: 'string',
        title: 'Caché',
      },

      extras: {
        type: 'string',
        title: 'Extras',
      },


      copyRight: {
        type: 'object',
        title: 'Derechos de imagen',
        properties: {
            description: {
                type: 'string',
                title: 'Derechos de imagen',
            },
    
            country: {
                type: 'string',
                title: 'País',
            },

            expiration: {
                 type: 'string',
                title: 'Caducidad de derechos de imagen',
            },
    
        }
      },

      manager: {
        type: 'object',
        title: 'Agencia / Representante',
        properties: {
            name: {
                type: 'string',
                title: 'Nombre',
            },
    
            commission: {
                type: 'string',
                title: 'Comisión',
            },
    
        }
    },

    required: []
}
};
