export default {
    type: 'object',
    title: 'Redes sociales',
    properties: {
        facebook: {
            type: 'array',
            title: 'Facebook',
        },

        instagram: {
            type: 'array',
            title: 'Instagram',
        },

        twitter: {
            type: 'array',
            title: 'Twitter',
        }
    },

    required: []
};
