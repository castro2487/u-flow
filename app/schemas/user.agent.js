export default {
    type: 'object',
    title: 'Representantes/Agencias',
    properties: {
        agents: {
            type: 'array',
            title: 'Representante/Agencia',
            items: {
                type: 'object',
                properties: {
                    businessName: {
                        type: 'string',
                        title: 'Razón social'
                    },

                    guest: {
                        type: 'string',
                        title: 'Email'
                    },

                    tradeName: {
                        type: 'string',
                        title: 'Nombre comercial'
                    },

                    dni: {
                        type: 'string',
                        title: 'DNI/CIF'
                    },

                    address: {
                        type: 'string',
                        title: 'Dirección'
                    },

                    phone: {
                        type: 'string',
                        title: 'Teléfono'
                    },

                    country: {
                        type: 'string',
                        title: 'Regiones/Paises'
                    },

                    email: {
                        type: 'string',
                        title: 'Email'
                    },

                    revenue: {
                        type: 'string',
                        title: '% revenue share'
                    },

                    commission: {
                        type: 'string',
                        title: '% comisión'
                    },

                    account: {
                        type: 'string',
                        title: 'N de cuenta'
                    }
                }
            }
        }
    },

    required: []
};
