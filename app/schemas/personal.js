import tutor from './tutor'
import address from './address'

export default {
    type: 'object',
    title: 'Información básica',
    properties: {
        name: {
            type: 'string',
            maxLength: 120,
            title: 'Nombre'
        },

        lastName: {
            type: 'string',
            maxLength: 120,
            title: 'Apellido'
        },

        sndLastName: {
            type: 'string',
            maxLength: 120,
            title: 'Segundo apellido'
        },

        dni: {
            type: 'string',
            minLength: 6,
            maxLength: 18,
            title: 'DNI',
            validationMessage: 'Campo inválido >> (000000R)'
        },

        nickName: {
            type: 'string',
            maxLength: 120,
            title: 'apodo'
        },

        bio: {
            type: 'string',
            maxLength: 220,
            title: 'Bio'
        },

        addresses: {
            type: 'array',
            title: 'Direcciones',
            minItems: 1,
            items: address
        },

        areaCode: {
            type: 'string',
            title: 'Área',
            default: '+34'
        },

        phone: {
            type: 'string',
            title: 'Teléfono'
        },

        picture: {
            type: 'string',
            title: 'Foto'
        },

        gender: {
            type: 'number',
            default: 0,
            title: 'Género'
        },

        picture: {
            type: 'string',
            title: 'Foto'
        },

        birthday: {
            type: 'string',
            title: 'Fecha de nacimiento'
        },

        sector: {
            type: 'array',
            title: 'Sector'
        },

        work: {
            type: 'array',
            title: 'Tipos de trabajo'
        },

        rol: {
            type: 'array',
            title: 'Rol'
        },

        tutor
    },

    required: ['name', 'lastName', 'dni', 'addresses', 'gender', 'tutor', 'birthday', 'picture']
}
