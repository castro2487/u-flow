export default {
    type: 'object',
    title: 'Información de cobro',
    properties: {
        account: {
            type: 'string',
            maxLength: 120,
            title: 'Número de cuenta'
        }
    }
}
