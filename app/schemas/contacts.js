export default {
    type: 'object',
    title: 'Persona de contacto',
    properties: {
        contacts: {
            type: 'array',
            title: 'Persona de contacto',
            items: {
                type: 'object',
                properties: {
                    name: {
                        type: 'string',
                        maxLength: 120,
                        title: 'Nombre comercial'
                    },

                    lastName: {
                        type: 'string',
                        maxLength: 120,
                        title: 'Apellido'
                    },

                    cargo: {
                        type: 'string',
                        title: 'cargo'
                    },

                    email: {
                        type: 'string',
                        title: 'email'
                    },

                    areaCode: {
                        type: 'string',
                        title: 'Área',
                        default: '+34'
                    },

                    phone: {
                        type: 'string',
                        title: 'Teléfono'
                    },
                    
                    birthday: {
                        type: 'string',
                        title: 'Fecha de nacimiento'
                    }
                },
            }
        }
    },
}
