export default {
    type: 'object',
    title: 'Acuerdos generales',
    properties: {
        commission: {
            type: 'string',
            title: '% comisión'
        },

        startDate: {
            type: 'string',
            title: 'Fecha de inicio'
        },

        endDate: {
            type: 'string',
            title: 'Fecha de expedición'
        },

        country: {
            type: 'array',
            title: 'Paises en los que está en vigor'
        },
    },

    required: ['commission', 'startDate', 'endDate', 'country']
};
