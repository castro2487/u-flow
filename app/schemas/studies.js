export default {
    type: 'object',
    title: 'Estudios',
    properties: {
        studies: {
            type: 'object',
            properties: {
                
                languages: {
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            
                            language: {
                                title: 'Idioma',
                                type: 'string'
                            },

                            level: {
                                title: 'Nivel',
                                type: 'string'
                            },

                            attachment: {
                                title: 'Demostración de título',
                                type: 'string'
                            },

                            video: {
                                title: 'Demostración con video',
                                type: 'string'
                            }
                        }
                    }
                },

                degree: {
                    type: 'object',

                    properties: {
                        title: {
                            title: 'Titulo',
                            type: 'string'
                        },

                        continueStudying: {
                            title: '¿Planeas seguir estudiando?',
                            type: 'string'
                        },

                        attachment: {
                            title: 'Añade una foto o PDF del título',
                            type: 'string'
                        },

                        video: {
                            title: 'Añade un vídeo que demuestre tu habilidad',
                            type: 'string'
                        }
                    }
                }
            },

            //required: []
        }
    }
}
