export default {
    type: 'object',
    title: 'Agencia / representantes',
    properties: {
        name: {
            type: 'string',
            maxLength: 120,
            title: 'Nombre comercial'
        },

        razonSocial: {
            type: 'string',
            maxLength: 120,
            title: 'Razón social'
        },

        dni: {
            type: 'string',
            minLength: 6,
            maxLength: 18,
            title: 'DNI / CIF'
        },

        country: {
            type: 'string',
            title: 'País'
        },

        address: {
            type: 'string',
            title: 'Dirección principal'
        },

        code: {
            type: 'string',
            title: 'Código Postal'
        },

        city: {
            type: 'string',
            title: 'Ciudad'
        },

        province: {
            type: 'string',
            title: 'Provincia'
        },

        areaCode: {
            type: 'string',
            title: 'Área',
            default: '+34'
        },

        phone: {
            type: 'string',
            title: 'Teléfono'
        },

        picture: {
            type: 'string',
            title: 'Foto'
        },

        gender: {
            type: 'number',
            default: 0,
            title: 'Género'
        },
    },

    required: ['name', 'razonSocial', 'dni', 'country', 'picture']
}
