const fieldZone = {
    fieldZone: {
        type: 'number'
    },

    attchment: {
        type: 'string',
        title: 'add_demostration_video'
    }
}

export default {
    type: 'object',
    properties: {
        behaviorPosition: {
            type: 'array',
            items: {
                type: 'object',
                title: 'behavior.title',
                properties: {
                    position: {
                        type: 'string',
                        title: 'behavior.position'
                    },

                    fieldAreaHighlight: {
                        type: 'object',
                        title: 'behavior.featuredPosition',
                        properties: fieldZone
                    },

                    fieldAreaSpendMoreTime: {
                        type: 'object',
                        title: 'behavior.field_more_time',
                        properties: fieldZone
                    },

                    fieldAreaLikeToSpendMoreTime: {
                        type: 'object',
                        title: 'behavior.field_like_to_be_more_time',
                        properties: fieldZone
                    },

                    fieldAreaMostReceiveTheBall: {
                        type: 'object',
                        title: 'behavior.field_get_ball',
                        properties: fieldZone
                    },

                    fieldAreaMostRecoverTheBall: {
                        type: 'object',
                        title: 'behavior.field_recover_ball',
                        properties: fieldZone
                    },

                    fieldAreaMostHittingTheBall: {
                        type: 'object',
                        title: 'behavior.field_hit_ball',
                        properties: fieldZone
                    }
                }
            }
        },
        required: []
    }
}
