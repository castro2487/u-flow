import tutor from './tutor'
import userAgency from './user.agent'

export default {
    type: 'object',
    title: 'Información basica',
    properties: {
        picture: {
            type: 'string',
            title: 'Foto'
        },

        name: {
            type: 'string',
            maxLength: 120,
            title: 'Nombre'
        },

        lastName: {
            type: 'string',
            maxLength: 120,
            title: 'Apellido'
        },

        sndLastName: {
            type: 'string',
            maxLength: 120,
            title: 'Segundo apellido'
        },

        nickName: {
            type: 'string',
            maxLength: 120,
            title: 'apodo'
        },

        gender: {
            type: 'number',
            default: 0,
            title: 'Genero'
        },

        birthday: {
            type: 'string',
            title: 'Fecha de nacimiento'
        },

        weight: {
            type: 'string',
            maxLength: 220,
            title: 'Peso'
        },

        height: {
            type: 'string',
            maxLength: 220,
            title: 'Altura'
        },

        nationality: {
            type: 'string',
            title: 'Nacionalidad'
        },

        country: {
            type: 'string',
            title: 'País'
        },

        city: {
            type: 'string',
            title: 'Ciudad'
        },

        sport: {
            type: 'string',
            title: 'Deporte'
        },

        dni: {
            type: 'string',
            minLength: 6,
            maxLength: 18,
            title: 'DNI',
            validationMessage: 'Campo inválido >> (000000R)'
        },

        tutor,

        agents:  {
            type: 'array',
            title: 'Direcciones',
            items: userAgency
        },
    },

    required: ['name', 'lastName', 'dni', 'gender', 'birthday', 'picture']
}
