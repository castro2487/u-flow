export default {
    type: 'object',
    title: 'Temporadas',
    properties: {
        experiences : {
            type:'array',
            title:'Temporadas',
            items: {
                    type: 'object',
                    title: 'Temporadas',
                    properties: {
                                season: {
                                    type: 'object',
                                    title: 'Temporada',
                                    properties: {
                                        year: {
                                            type: 'number',
                                            default:1989
                                        },
                                        semester: {
                                            type: 'number',
                                            default:0
                                        },
                                        month: {
                                            type: 'number',
                                            default:1
                                        },
                                    }
                                },
                                clubName: {
                                    type: 'string',
                                    default:''
                                },
                                clubLogo: {
                                    type: 'string',
                                    default:''
                                },
                                totalTeamMatchs: {
                                    type: 'number',
                                    default:0
                                },
                                totalPlayerMatchs: {
                                    type: 'number',
                                    default:0
                                },
                                minutesPlayed: {
                                    type: 'number',
                                    default:0
                                },
                                totalGoals: {
                                    type: 'number',
                                    default:0
                                },
                                totalKeyPasses: {
                                    type: 'number',
                                    default:0
                                },
                                totalYellowCards: {
                                    type: 'number',
                                    default:0
                                },
                                totalRedCards: {
                                    type: 'number',
                                    default:0
                                },
                                ranking: {
                                    type: 'number',
                                    default:0
                                },
                                trainner: {
                                    type: 'string',
                                    default:''
                                },
                    },
                    required: []
            }
        }
                
    },

    required: []
};
