export default {
    type: 'object',
    title: 'Información basica',
    properties: {

        mainSkills: {
            type: 'object',
            title: 'Habilidades principales',
            properties: {
                control: {
                    type: 'number',
                    title: 'Control de balón',
                    default:0
                },
                shooting: {
                    type: 'number',
                    title: 'Habilidad de disparo',
                    default:0
                },
                head: {
                    type: 'number',
                    title: 'Juego de Cabeza',
                    default:0
                },
                strategy: {
                    type: 'number',
                    title: 'Construcción del juego',
                    default:0
                },
                passing: {
                    type: 'number',
                    title: 'Pases',
                    default:0
                },
                lateralPlay: {
                    type: 'number',
                    title: 'Jugadas por las bandas',
                    default:0
                },
                finishing: {
                    type: 'number',
                    title: 'Finalización',
                    default:0
                },
                leftLeg: {
                    type: 'number',
                    title: 'Pie izquierdo',
                    default:0
                },
                rightLeg: {
                    type: 'number',
                    title: 'Pie derecho',
                    default:0
                },
                /*
                videos: {
                    type: 'array',
                    title: 'Videos',
                    items: {
                        type: 'object',
                        properties: {
                            url: {
                                title: 'URL',
                                type: 'string'
                            },
                            poster: {
                                title: 'Cover',
                                type: 'string'
                            },
                            type: {
                                title: 'Type',
                                type: 'string'
                            }
                        }
                    }
                },*/
            }
        },

        tacticSkills: {
            type: 'object',
            title: 'Habilidades tácticas',
            properties: {
                vision: {
                    type: 'number',
                    title: 'Visión',
                    default:0
                },
                awareness: {
                    type: 'number',
                    title: 'Conciencia',
                    default:0
                },
                ballMoviment: {
                    type: 'number',
                    title: 'Movimiento del balón',
                    default:0
                },
                /*
                videos: {
                    type: 'array',
                    title: 'Videos',
                    items: {
                        type: 'object',
                        properties: {
                            url: {
                                title: 'URL',
                                type: 'string'
                            },
                            poster: {
                                title: 'Cover',
                                type: 'string'
                            },
                            type: {
                                title: 'Type',
                                type: 'string'
                            }
                        }
                    }
                },*/
            }
        },

        athleticSkills: {
            type: 'object',
            title: 'Habilidades atléticas',
            properties: {
                speed: {
                    type: 'number',
                    title: 'Velocidad',
                    default:0
                },
                acuity: {
                    type: 'number',
                    title: 'Agudeza',
                    default:0
                },
                mobility: {
                    type: 'number',
                    title: 'Movilidad',
                    default:0
                },
                bodyStrength: {
                    type: 'number',
                    title: 'Fuerza corporal',
                    default:0
                },
                rhythm: {
                    type: 'number',
                    title: 'Ritmo de trabajo',
                    default:0
                },
                /*
                videos: {
                    type: 'array',
                    title: 'Videos',
                    items: {
                        type: 'object',
                        properties: {
                            url: {
                                title: 'URL',
                                type: 'string'
                            },
                            poster: {
                                title: 'Cover',
                                type: 'string'
                            },
                            type: {
                                title: 'Type',
                                type: 'string'
                            }
                        }
                    }
                },*/
            }
        },
        
        character: {
            type: 'object',
            title: 'Habilidades',
            properties: {
                recovery: {
                    type: 'number',
                    title: 'Esfuerzo para recuperar el balón',
                    default:0
                },
                physicalBravery: {
                    type: 'number',
                    title: 'Valentía física',
                    default:0
                },
                mentalBravery: {
                    type: 'number',
                    title: 'Valentía mental',
                    default:0
                },
                energy: {
                    type: 'number',
                    title: 'Energía',
                    default:0
                },
                leadership: {
                    type: 'number',
                    title: 'Liderazgo',
                    default:0
                },
                communication: {
                    type: 'number',
                    title: 'Comunicación',
                    default:0
                },
                /*
                videos: {
                    type: 'array',
                    title: 'Videos',
                    items: {
                        type: 'object',
                        properties: {
                            url: {
                                title: 'URL',
                                type: 'string'
                            },
                            poster: {
                                title: 'Cover',
                                type: 'string'
                            },
                            type: {
                                title: 'Type',
                                type: 'string'
                            }
                        }
                    }
                },*/
            },
        },
    },

    required: []
};
