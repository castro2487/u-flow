import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import Layout from './views/Layout'
import PageNotFound from './views/PageNotFound'
import { debug } from './config/_constants'

Vue.use(Router)

export const isAuthenticated = id =>
    (!!store.getters['Auth/isAuthenticated'] && store.getters['Auth/isCurrentUser'](id)) || debug

export const ifNotAuthenticated = (to, from, next) => {
    if (isAuthenticated()) {
        next()
        return
    }
    next('/')
}

export const ifAuthenticated = (to, from, next) => {
    if (isAuthenticated(to.params.id)) {
        next()
        return
    }
    next('/')
}

export const checkProfile = (id, next, to) => {
    if (isAuthenticated(id)) {
        if (store.getters['Auth/isCurrentUser'](id)) {
            if (!store.getters['User/hasProfileCreated'] && to.name !== 'CreateProfile') {
                return next({ name: 'CreateProfile', params: { id } })
            }
        }
    }

    return next()
}

export const hasUncompletedProfile = (to, from, next) => {
    const id = to.params.id
    if (store.state.User.me.id === id) {
        return checkProfile(id, next, to)
    } else {
        store.dispatch('User/GetUser', id).then(response => {
            if (response.status.code === 400) {
                next('/')
            } else {
                return checkProfile(id, next, to)
            }
        })
    }
}

export const ifEmailRequired = (to, from, next) => {
    if (store.getters['Auth/hasEmail']) {
        next()
        return
    }
    next('/')
}

export const managerRequired = (to, from, next) => {
    if (isAuthenticated(to.params.id) && store.getters['Auth/user'].role === 'manager') {
        next()
        return
    }
    next('/')
}

export const verifyIsLogged = (to, from, next) => {
    if (store.dispatch('Auth/AutoLogged')) {
        const user = store.getters['Auth/user']
        const currentId = from.params.id

        if (!currentId && user && user.id) {
            next({ name: 'Portada', params: { id: user.id } })
            return
        }
    }
    next()
}

export const LogOutUser = (to, from, next) => {
    store.dispatch('Auth/LogOut')
    next('/')
}

const router = new Router({
    routes: [
        {
            path: '/verifications/:token',
            name: 'Confirm',
            component: () => import(/* webpackChunkName: "Confirm" */ './views/Confirm')
        },
        {
            path: '/signIn',
            name: 'SignIn',
            beforeEnter: ifEmailRequired,
            component: () => import(/* webpackChunkName: "SignIn" */ './views/SignIn')
        },
        {
            path: '/signUp',
            name: 'SignUp',
            beforeEnter: ifEmailRequired,
            component: () => import(/* webpackChunkName: "SignUp" */ './views/SignUp')
        },
        {
            path: '/:email?',
            name: 'Sign',
            component: () => import(/* webpackChunkName: "Sign" */ './views/Sign'),
            beforeEnter: verifyIsLogged
        },
        {
            path: '/forgot',
            name: 'Forgot',
            beforeEnter: ifEmailRequired,
            component: () => import(/* webpackChunkName: "Forgot" */ './views/Forgot')
        },
        {
            path: '/newPassword/:token',
            name: 'NewPassword',
            component: () => import(/* webpackChunkName: "NewPassword" */ './views/NewPassword')
        },

        {
            path: '/user/:id',
            name: 'Talent',
            component: Layout,
            beforeEnter: hasUncompletedProfile,
            children: [
                {
                    path: '',
                    name: 'Portada',
                    component: () =>
                        import(/* webpackChunkName: "Portfolio" */ './views/Home/index.vue')
                },

                {
                    path: 'works',
                    name: 'Works',
                    component: () =>
                        import(/* webpackChunkName: "WorkList" */ './views/Work/WorkList.vue')
                },

                {
                    path: 'work/:workId',
                    name: 'Work',
                    component: () =>
                        import(/* webpackChunkName: "WorkView" */ './views/Work/WorkView.vue')
                },

                {
                    path: 'editoriales',
                    name: 'Editoriales'
                    //component: () => import(/* webpackChunkName: "Editoriales" */ './views/Editoriales.vue')
                },

                {
                    path: 'pasarela',
                    name: 'Pasarela'
                    //component: () => import(/* webpackChunkName: "Pasarela" */ './views/Pasarela.vue')
                },

                {
                    path: 'publicidad',
                    name: 'Publicidad'
                    //component: () => import(/* webpackChunkName: "Publicidad" */ './views/Publicidad.vue')
                },

                {
                    path: 'about',
                    name: 'About',
                    component: () => import(/* webpackChunkName: "About" */ './views/About')
                },

                {
                    path: 'contracts',
                    name: 'Contracts',
                    beforeEnter: ifAuthenticated,
                    component: () =>
                        import(/* webpackChunkName: "Contracts" */ './views/Contract/ContractList.vue')
                },

                {
                    path: 'contract/:contractId',
                    name: 'ContractInfo',
                    beforeEnter: ifAuthenticated,
                    component: () =>
                        import(/* webpackChunkName: "ManagerContract" */ './views/Contract/ContractInfo.vue')
                },
                // CREA TU PERFIL
                {
                    path: 'account/profile/Create',
                    name: 'CreateProfile',
                    beforeEnter: ifAuthenticated,
                    component: () =>
                        import(/* webpackChunkName: "CreateProfile" */ './views/Profile/CreatePlayoneer.vue')
                },
                // COMPLETA TU PERFIL
                {
                    path: 'account/profile/Edit/:step',
                    name: 'EditProfile',
                    beforeEnter: ifAuthenticated,
                    component: () =>
                        import(/* webpackChunkName: "EditProfile" */ './views/Profile/EditPlayoneer.vue')
                },

                {
                    path: 'account/work/Edit/:workId',
                    name: 'EditWork',
                    beforeEnter: ifAuthenticated,
                    component: () =>
                        import(/* webpackChunkName: "EditWork" */ './views/Work/WorkEdit.vue')
                },

                {
                    path: 'account/work/Create',
                    name: 'CreateWork',
                    beforeEnter: ifAuthenticated,
                    component: () =>
                        import(/* webpackChunkName: "CreateWork" */ './views/Work/WorkCreate.vue')
                },

                {
                    path: 'account/contract/:contractId/Create',
                    name: 'ContractCreate',
                    beforeEnter: managerRequired,
                    component: () =>
                        import(/* webpackChunkName: "ContractCreate" */ './views/Contract/ContractForm.vue')
                },

                {
                    path: 'account/contract/:contractId/Edit',
                    name: 'ContractEdit',
                    beforeEnter: managerRequired,
                    component: () =>
                        import(/* webpackChunkName: "ContractEdit" */ './views/Contract/ContractForm.vue')
                }
            ]
        },
        {
            path: '/logOut',
            name: 'LogOut',
            beforeEnter: LogOutUser
        },
        {
            path: '*',
            redirect: { name: 'Page404' }
        },
        {
            path: '/404',
            name: 'Page404',
            component: PageNotFound,
            alias: '404'
        }
    ]
})

export default router
