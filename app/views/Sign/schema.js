export default {
  type: 'object',
  title: 'Forma parte de Playonner',
  properties: {

    // Verificar si se puede cambiar el email
    email: {
      type: 'email',
      maxLength: 120,
      title: '',
      field: {
        type: 'email',
        placeholder: '...',
        'data-predictible-text': 'introduce tu email'
      },
      errorMessage: {
        required: ' ',
        email: ' '
      }
    },
  },

  additionalProperties: false,
  required: ['email', 'password']
};