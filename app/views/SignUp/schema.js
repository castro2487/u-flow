
export default {
  type: 'object',
  title: 'Forma parte de Playonner',
  properties: {

    password: {
      type: 'string',
      title: 'Password',
      minLength: 8,
      default: '',
      field: {
        type: 'password',
        'data-predictible-text': 'crea una nueva contraseña',
      },
     
      validator(form, rule, value, callback) {
        const confirmValue = form.getFieldValue('confirmPassword');
        if (value && confirmValue) {
          form.validateFields(['confirmPassword'], { force: true });
        }
        callback();
      }
    },

    confirmPassword: {
      type: 'string',
      title: 'Confirm Password',
      default: '',
      field: {
        type: 'password',
        'data-predictible-text': 'repite tu contraseña',
      },
     
      validator(form, rule, value, callback) {
        if (value && value !== form.getFieldValue('password')) {
          callback('Las contraseñas deben ser identicas');
        } else {
          callback();
        }
      },
      hidden(model) {
        return !model.password || model.password.length < 8;
      },
    }

  },
  additionalProperties: false,
  required: ['email', 'password', 'confirmPassword']
};
