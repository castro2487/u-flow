export default {
  type: 'object',
  title: 'Forma parte de Playonner',
  properties: {

    password: {
      type: 'string',
      title: 'Contraseña',
      minLength: 8,
      field: {
        type: 'password',
        'data-predictible-text': 'introduce tu contraseña'
      },
    },

  },

  additionalProperties: false,
  required: ['email', 'password']
};
