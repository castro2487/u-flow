export default {
  type: 'object',
  title: 'Forma parte de Playonner',
  properties: {

    email: {
      type: 'email',
      maxLength: 120,
      title: 'Email',
      default: '',
      field: {
        type: 'email',
        placeholder: 'Your Email'
      },
      errorMessage: {
        required: 'Please enter your email'
      }
    },

  },
  additionalProperties: false,
  required: ['email']
};
