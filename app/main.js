import Vue from 'vue'
import Antd from 'ant-design-vue'
import App from './App.vue'
import i18n from './utils/Translator'
import store from './store/'
import router from './router'
import VueDND from 'awe-dnd'
import { VueMasonryPlugin } from 'vue-masonry'

import 'ant-design-vue/dist/antd.css'
import 'assets/scss/index.scss'

Vue.use(VueDND)
Vue.config.productionTip = false

Vue.use(VueMasonryPlugin)
Vue.use(Antd)

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app')
