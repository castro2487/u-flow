/*
    Datos basicos
    Representantes
*/
import InfoSchema from 'schemas/personalPlayoneer'
import InfoForm from './personalPlayoneer.form'

import agentsSchema from 'schemas/agent'
import agentsForm from './user.agents.form'

export default [
    {
        label: 'Info',
        schema: InfoSchema,
        form: InfoForm
    },/*
    {
        label: 'Representantes',
        schema: agentsSchema,
        form: agentsForm
    }*/
]
