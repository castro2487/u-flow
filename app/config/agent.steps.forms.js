import InfoSchema from 'schemas/agent'
import socialSchema from 'schemas/social'
import accountSchema from 'schemas/account'
import contactsSchema from 'schemas/contacts'

import infoForm from './agent.form'
import accountForm from './account.form'
import contactsForm from './contacts.form'
import socialForm from './social.form'

export default [
    {
        label: 'Info',
        schema: InfoSchema,
        form: infoForm
    },
    {
        label: 'social',
        schema: socialSchema,
        form: socialForm
    },
    {
        label: 'Personas de contacto',
        schema: contactsSchema,
        form: contactsForm
    },
    {
        label: 'Datos de facturación',
        schema: accountSchema,
        form: accountForm
    }
]
