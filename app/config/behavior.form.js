import Store from 'store'

export default [
    {
        key: 'behaviorPosition',
        title: ' Comportamiento en el campo',
        removeLabel: 'Remover posición alternativa',
        buttonLabel: 'Añadir una posición alternativa',
        maxItems: 2,
        fields: [
            {
                key: 'behaviorPosition[].position',
                type: 'select',
                valueKey: 'label',
                placeholder: '...',
                titleMap: Store.state.Miscs.Positions
            },
            {
                type: 'secuency',
                nextLabel: 'siguiente',
                prevLabel: 'anterior',
                condition({behaviorPosition}, form, {currentIndex}) {
                    const position = (behaviorPosition[currentIndex] || {}).position || ''
                    return position.length > 3
                },
                fields: [
                    {
                        type: 'fieldset',
                        title: 'Indica en qué parte del campo destacas',
                        fields: [
                            {
                                key: 'behaviorPosition[].fieldAreaHighlight.fieldZone',
                                type: 'area',
                                hideTitle: true
                            },
                            {
                                key: 'behaviorPosition[].fieldAreaHighlight.attchment',
                                type: 'uploadVideo',
                                title: 'Añade un vídeo que demuestre tu habilidad',
                                action: (data) => Store.dispatch('Miscs/UploadFile', data),
                            }
                        ]
                    },

                    {
                        type: 'fieldset',
                        title: 'Indica en qué parte del campo estás más tiempo',
                        fields: [
                            {
                                key: 'behaviorPosition[].fieldAreaSpendMoreTime.fieldZone',
                                type: 'area',
                                hideTitle: true
                            },
                            {
                                key: 'behaviorPosition[].fieldAreaSpendMoreTime.attchment',
                                type: 'uploadVideo',
                                title: 'Añade un vídeo que demuestre tu habilidad',
                                action: (data) => Store.dispatch('Miscs/UploadFile', data),
                            }
                        ]
                    },

                    {
                        type: 'fieldset',
                        title: 'Indica en qué parte del campo te gusta estar más tiempo',
                        fields: [
                            {
                                key: 'behaviorPosition[].fieldAreaLikeToSpendMoreTime.fieldZone',
                                type: 'area',
                                hideTitle: true
                            },
                            {
                                key: 'behaviorPosition[].fieldAreaLikeToSpendMoreTime.attchment',
                                type: 'uploadVideo',
                                title: 'Añade un vídeo que demuestre tu habilidad',
                                action: (data) => Store.dispatch('Miscs/UploadFile', data),
                            }
                        ]
                    },

                    {
                        type: 'fieldset',
                        title: 'Indica en qué parte del campo recibes más el balón',
                        fields: [
                            {
                                key: 'behaviorPosition[].fieldAreaMostReceiveTheBall.fieldZone',
                                type: 'area',
                                hideTitle: true
                            },
                            {
                                key: 'behaviorPosition[].fieldAreaMostReceiveTheBall.attchment',
                                type: 'uploadVideo',
                                title: 'Añade un vídeo que demuestre tu habilidad',
                                action: (data) => Store.dispatch('Miscs/UploadFile', data),
                            }
                        ]
                    },

                    {
                        type: 'fieldset',
                        title: 'Indica en qué parte del campo recuperas más el balón',
                        fields: [
                            {
                                key: 'behaviorPosition[].fieldAreaMostRecoverTheBall.fieldZone',
                                type: 'area',
                                hideTitle: true
                            },
                            {
                                key: 'behaviorPosition[].fieldAreaMostRecoverTheBall.attchment',
                                type: 'uploadVideo',
                                title: 'Añade un vídeo que demuestre tu habilidad',
                                action: (data) => Store.dispatch('Miscs/UploadFile', data),
                            }
                        ]
                    },

                    {
                        type: 'fieldset',
                        title: 'Indica en qué parte del campo golpeas más el balón',
                        fields: [
                            {
                                key: 'behaviorPosition[].fieldAreaMostHittingTheBall.fieldZone',
                                type: 'area',
                                hideTitle: true
                            },
                            {
                                key: 'behaviorPosition[].fieldAreaMostHittingTheBall.attchment',
                                type: 'uploadVideo',
                                title: 'Añade un vídeo que demuestre tu habilidad',
                                action: (data) => Store.dispatch('Miscs/UploadFile', data),
                            }
                        ]
                    }
                ]
            }
        ]
    }
]
