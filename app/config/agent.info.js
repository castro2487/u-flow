import Store from 'store'
import { getYearsOld } from 'utils/DateUtils'
export default [
    {
        title: 'Información básica',
        properties: [
            {
                key: 'razonSocial',
                title: 'Razón Social'
            },

            {
                key: 'name',
                title: 'Nombre comercial'
            },

            {
                key: 'dni',
                title: 'DNI / CIF'
            },

            {
                key: 'country',
                title: 'País'
            },

            {
                key: 'custom',
                title: 'Dirección'
            }
        ]
    },
    {
        title: 'Redes sociales',
        properties: [
            {
                key: 'facebook',
                title: 'Facebook',
                type: 'list'
            },
            {
                key: 'instagram',
                title: 'Instagram',
                type: 'list'
            },
            {
                key: 'twitter',
                title: 'Twitter',
                type: 'list'
            }
        ]
    },
    {
        title: 'Personas de contacto',
        properties: [
            {
                key: 'name',
                type: 'string',
                title: 'Nombre'
            },

            {
                key: 'lastName',
                type: 'string',
                title: 'Apellido'
            },

            {
                key: 'cargo',
                type: 'string',
                title: 'Cargo'
            },

            {
                key: 'email',
                type: 'string',
                title: 'Email'
            },
            {
                key: 'decorator',
                title: 'Teléfono',
                compute(model) {
                    return model.phone ? `${model.areaCode} ${model.phone || ''}` : ''
                }
            },
            {
                key: 'birthday',
                type: 'string',
                title: 'Fecha de nacimiento'
            }
        ]
    },
    {
        title: 'Datos de facturación',
        properties: [
            {
                key: 'account',
                title: 'Número de cuenta'
            }
        ]
    }
]
