
import {uniqBy} from 'lodash/array';
import {getApiUrl} from 'utils/ClientServices';
import Store from 'store';
import {hasLegalAge} from 'utils/helpers';


export default [
    {
        key: 'picture',
        type: 'chooseImage',
        title: 'Elegir foto de perfil',
        className: 'cell-12 margin-top-2',
        minSize: 0.1525,
        maxSize: 20,
        action: (data) => Store.dispatch('Miscs/UploadFile', data),
        attr: {        
          accept: 'image/png, image/jpeg',
          listType: 'picture-card'
        },
  
    },

    'name',
    'lastName',
    'nickname',

    {
        key: 'gender',
        type: 'select',
        attr: {
          placeholder: '...'
        },
        titleMap: [
          {value: 0, label: 'Hombre'},
          {value: 1, label: 'Mujer'},
          {value: 2, label: 'Neutro'},
        ]
      },
  
      { 
        key:'birthday',
        title: 'Fecha de nacimiento',
        type: 'dateSelect',
      }, 

    {
        key: 'weight',
    },


    {
        key: 'height',
    },

    {
        key: 'nationality',
    },

    {
        key: 'country',
        type: 'select',
        valueKey: 'alpha2Code',
        labelKey: 'name',
        asyncPopulate: function (data) {
        return new Promise((resolve, reject) => {
            //Se verifica que existan paises
            // console.log('COUNTRIES >> ',Store.state.Miscs.countries)
            if (Store.state.Miscs.countries.length) {
            resolve({ titleMap: Store.state.Miscs.countries })
            }
            else {
            // console.log('GetCountries')
            Store.dispatch('Miscs/GetCountries')
                    .then( response => resolve({ titleMap: response.countries }))
            }

        })
        }
    },

    {
        key: 'city',
    },

    {
        key: 'sport',
        type: 'select',
        titleMap: [
            { value: 'futbol', label: 'Fútbol' }
        ]
    },

    //...AgentForm,

    {
      key: 'idnumber',
    },


  ]
