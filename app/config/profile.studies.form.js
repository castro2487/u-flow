import Store from 'store'

export default [
    {
        type: 'fieldset',
        title: 'Educación',
        fields: [
            {
                key: 'studies.degree.title',
                type: 'select',
                title: 'Nivel de estudios',
                valueKey: 'label',
                titleMap: Store.state.Miscs.Studies
            },
            {
                key: 'studies.degree.continueStudying',
                type: 'select',
                title: '¿Planeas seguir estudiando?',
                titleMap: Store.state.Miscs.YesorNot
            },

            {
                key: 'studies.degree.attachment',
                type: 'uploadVideo',
                title: 'Añade imagen o PDF del titulo',
                action: data => Store.dispatch('Miscs/UploadFile', data)
            },

            {
                key: 'studies.degree.video',
                type: 'uploadVideo',
                title: 'Añade un vídeo que demuestre tu habilidad',
                action: data => Store.dispatch('Library/Add', data)
            }
        ]
    },

    {
        key: 'studies.languages',
        type: 'array',
        removeLabel: 'Remover idioma',
        buttonLabel: 'Añadir otro idioma',
        titleItem(index) {
            return `Idioma`
        },
        fields: [
            {
                key: 'studies.languages[].language',
                type: 'select',
                valueKey: 'label',
                titleMap: Store.state.Miscs.Language
            },
            {
                key: 'studies.languages[].level',
                type: 'select',
                valueKey: 'label',
                titleMap: Store.state.Miscs.ExpertiseLevel
            },

            {
                key: 'studies.languages[].video',
                type: 'uploadVideo',
                title: 'Añade un vídeo que demuestre tu habilidad',
                action: data => Store.dispatch('Library/Add', data)
            }
        ]
    }
]
