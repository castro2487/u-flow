export default [
    {
        key: 'facebook',
        type: 'social',
        icon: 'facebook',
        className: 'cell-12 padding-y-1',
        buttonLabel (total) {
            return !!total
                    ? 'Añadir otra cuenta de Facebook'
                    : 'Añadir una cuenta de Facebook'
        } 
    },

    {
        key: 'instagram',
        type: 'social',
        icon: 'instagram',
        className: 'cell-12 padding-y-1',
        buttonLabel (total) {
            return !!total
                    ? 'Añadir otra cuenta de Instagram'
                    : 'Añadir una cuenta de Instagram'
        } 
    },

    {
        key: 'twitter',
        type: 'social',
        icon: 'twitter',
        className: 'cell-12 padding-y-1',
        buttonLabel (total) {
            return !!total
                    ? 'Añadir otra cuenta de Twitter'
                    : 'Añadir una cuenta de Twitter'
        } 
    }
];
