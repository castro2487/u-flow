
import Store from 'store';

export default [
    {
        type: 'fieldset',
        title: 'Habilidades principales',
        titleClassName: 'size-4 margin-y margin-bottom-0',
        fields: [{
            key: 'mainSkills.control',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'mainSkills.shooting',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'mainSkills.head',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'mainSkills.strategy',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'mainSkills.passing',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'mainSkills.lateralPlay',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'mainSkills.finishing',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'mainSkills.leftLeg',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'mainSkills.rightLeg',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },/*
        {
            key: 'picture',
            type: 'chooseImage',
            title: 'Elegir foto de perfil',
            className: 'cell-12',
            minSize: 0.1525,
            maxSize: 20,
            action: (data) => Store.dispatch('Miscs/UploadFile', data),
            attr: {        
              accept: 'image/png, image/jpeg',
              listType: 'picture-card'
            },
        }*/]
    },

    { type: 'separator'},
    
    {
        type: 'fieldset',
        title: 'Habilidades tácticas',
        titleClassName: 'size-4 margin-y margin-bottom-0',
        fields: [
            {
                key: 'tacticSkills.vision',
                type: 'sliderSkill',
                className: 'cell-12',
                default:0
            },
            {
                key: 'tacticSkills.awareness',
                type: 'sliderSkill',
                className: 'cell-12',
                default:0
            },
            {
                key: 'tacticSkills.ballMoviment',
                type: 'sliderSkill',
                className: 'cell-12',
                default:0
            },
            /*
            {
                key: 'picture',
                type: 'chooseImage',
                title: 'Elegir foto de perfil',
                className: 'cell-12',
                minSize: 0.1525,
                maxSize: 20,
                action: (data) => Store.dispatch('Miscs/UploadFile', data),
                attr: {        
                accept: 'image/png, image/jpeg',
                listType: 'picture-card'
                },
            }*/
        ]
    },

    { type: 'separator'},

    {
        type: 'fieldset',
        title: 'Habilidades atléticas',
        titleClassName: 'size-4 margin-y margin-bottom-0',
        fields: [{
            key: 'athleticSkills.speed',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'athleticSkills.acuity',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'athleticSkills.mobility',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'athleticSkills.bodyStrength',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        {
            key: 'athleticSkills.rhythm',
            type: 'sliderSkill',
            className: 'cell-12',
            default:0
        },
        /*
        {
            key: 'picture',
            type: 'chooseImage',
            title: 'Elegir foto de perfil',
            className: 'cell-12',
            minSize: 0.1525,
            maxSize: 20,
            action: (data) => Store.dispatch('Miscs/UploadFile', data),
            attr: {        
              accept: 'image/png, image/jpeg',
              listType: 'picture-card'
            },
        }*/]
    },
  ]
