import Store from 'store'
import { hasLegalAge } from 'utils/helpers'
import userAgent from './user.agents.form'

export default [
    {
        key: 'picture',
        type: 'chooseImage',
        title: 'Elegir foto de perfil',
        className: 'cell-12 margin-top-2',
        minSize: 0.1525,
        maxSize: 20,
        action: data => Store.dispatch('Miscs/UploadFile', data),
        attr: {
            accept: 'image/png, image/jpeg',
            listType: 'picture-card'
        }
    },

    {
        key: 'name',
        default: '...'
    },

    {
        key: 'lastName',
        default: '...'
    },
    
    {
        key: 'nickname',
        default: '...'
    },

    {
        key: 'gender',
        type: 'select',
        attr: {
            placeholder: '...'
        },
        titleMap: [
            { value: 0, label: 'Hombre' },
            { value: 1, label: 'Mujer' },
            { value: 2, label: 'Neutro' }
        ]
    },

    {
        key: 'birthday',
        title: 'Fecha de nacimiento',
        type: 'dateSelect'
    },

    {
        type: 'fieldset',
        gridClassName: 'padding-x margin--x',
        condition: ({ birthday }) => birthday && !hasLegalAge(birthday),
        fields: [
            {
                type: 'help',
                className: 'color-pink-light cell-12 margin-top-1 margin-bottom-2 size-4',
                description:
                    'Los menores de edad, necesitan autorización expresa de un tutor legal.'
            },

            {
                key: 'tutor',
                titleClassName: 'font-weight-5'
            },

            { type: 'separator' }
        ]
    },

    {
        key: 'weight',
        default: '...',
        afterValue:'Kg',
        attr: {
            type: 'number'
        }
    },

    {
        key: 'height',
        default: '...',
        afterValue:'Cm',
        attr: {
            type: 'number'
        }
    },

    {
        key: 'nationality',
        default: '...'
    },

    {
        key: 'country',
        type: 'select',
        valueKey: 'alpha2Code',
        labelKey: 'name',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                //Se verifica que existan paises
                // console.log('COUNTRIES >> ',Store.state.Miscs.countries)
                if (Store.state.Miscs.countries.length) {
                    resolve({ titleMap: Store.state.Miscs.countries })
                } else {
                    // console.log('GetCountries')
                    Store.dispatch('Miscs/GetCountries').then(response =>
                        resolve({ titleMap: response.countries })
                    )
                }
            })
        }
    },

    {
        key: 'city',
        default: '...'
    },

    {
        key: 'sport',
        type: 'select',
        titleMap: [{ value: 'futbol', label: 'Fútbol' }]
    },

    //...userAgent,

    {
        key: 'dni',
        default: '...'
    }
]
