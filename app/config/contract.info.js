import moment from 'moment'
export default {
    title: 'Acuerdos generales',
    properties: [
        {
            key: 'comission',
            title: 'Comisión',
            compute(model, value = '') {
                return value ? `${value} %` : ''
            }
        },

        {
            key: 'startDate',
            title: 'Fecha de inicio',
            compute(model, value = '') {
                try {
                    return value.length ? moment(value).format('LL') : '...'
                } catch (e) {
                    return '...'
                }
            }
        },

        {
            key: 'endDate',
            title: 'Fecha de expedición',
            compute(model, value = '') {
                try {
                    return value.length ? moment(value).format('LL') : '...'
                } catch (e) {
                    return '...'
                }
            }
        },

        {
            key: 'country',
            type: 'string',
            title: 'Paises en los que está en vigor',
            compute(model, value, index) {
                return (value || []).map( item => item).toString() || '...'
            },
        }
    ]
}
