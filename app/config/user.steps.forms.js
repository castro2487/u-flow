import InfoSchema from 'schemas/personal'
import socialSchema from 'schemas/social'
import studiesSchema from 'schemas/studies'
import agentsSchema from 'schemas/agent'
import behaviorSchema from 'schemas/behavior'

import InfoForm from './personal.form'
import socialForm from './social.form'
import studiesForm from './profile.studies.form'
import agentsForm from './user.agents.form'
import behaviorForm from './behavior.form'

export default [
    {
        label: 'Info',
        schema: InfoSchema,
        form: InfoForm
    },
    {
        label: 'social',
        schema: socialSchema,
        form: socialForm
    },
    {
        label: 'studies',
        schema: studiesSchema,
        form: studiesForm
    },
    {
        label: 'Comportanmiento en el campo',
        schema: behaviorSchema,
        form: behaviorForm
    },
    {
        label: 'Habilidades',
        schema: {},
        form: []
    },
    {
        label: 'Temporada',
        schema: {},
        form: []
    }
]
