import Store from 'store'

export default [
    {
        key: 'etnia',
        type: 'select',
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.Etnia })
            })
        }
    },
    {
        key: 'height',
        afterValue: 'cm',
        default: 0,
        attr: {
            type: 'number'
        }
    },
    {
        key: 'weight',
        afterValue: 'kg',
        default: 0,
        attr: {
            type: 'number'
        }
    },
    {
        key: 'complexion',
        type: 'select',
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.Complexion })
            })
        }
    },

    {
        key: 'chestMeasurement',
        afterValue: 'cm',
        default: 0,
        attr: {
            type: 'number'
        }
    },

    {
        key: 'waistMeasurement',
        afterValue: 'cm',
        default: 0,
        attr: {
            type: 'number'
        }
    },

    {
        key: 'hipMeasurement',
        afterValue: 'cm',
        default: 0,
        attr: {
            type: 'number'
        }
    },

    {
        key: 'eyes',
        type: 'select',
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.EyesColor })
            })
        }
    },
    {
        key: 'hairColor',
        type: 'select',
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.HairColor })
            })
        }
    },
    {
        key: 'hairStyle',
        type: 'select',
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.HairStyle })
            })
        }
    },
    {
        key: 'hairLong',
        type: 'select',
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.HairLong })
            })
        }
    },
    {
        key: 'beard',
        type: 'select',
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.Beard })
            })
        }
    },
    {
        key: 'sizeShirt',
        type: 'selectCoord',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.SizeShoes[data.gender] })
            })
        }
    },
    {
        key: 'sizeJacket',
        type: 'selectCoord',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.SizeTop[data.gender] })
            })
        }
    },
    {
        key: 'sizeShoes',
        type: 'selectCoord',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.SizeUnder[data.gender || 0] })
            })
        }
    },
    {
        key: 'piercings',
        type: 'select',
        titleMap: [{ value: 'true', label: 'Sí' }, { value: 'false', label: 'No' }]
    },
    {
        key: 'tattoos',
        type: 'select',
        titleMap: [{ value: 'true', label: 'Sí' }, { value: 'false', label: 'No' }]
    },
    {
        key: 'peculiarities',
        type: 'textarea',
        attr: {
            placeholder: '¿Tienes alguna característica física que te gustaría resaltar?'
        }
    }
]
