import { uniqBy } from 'lodash/array'
import { getApiUrl } from 'utils/ClientServices'
import { range, hasLegalAge } from 'utils/helpers'
import Store from 'store'

export default [
    'title',
    {
        key: 'description',
        type: 'textarea',
        attr: {
            placeholder: 'Escribe unas breves lineas sobre el proyecto'
        }
    },

    {
        type: 'separator'
    },

    {
        key: 'sector',
        type: 'select',
        title: 'Sector',
        attr: {
            placeholder: 'Puedes eligir más de uno',
            mode: 'multiple'
        },
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.sector })
            })
        }
    },

    {
        key: 'work',
        type: 'select',
        title: 'Tipos de trabajo',
        attr: {
            placeholder: 'Puedes eligir más de uno',
            mode: 'multiple'
        },
        reactiveKey: 'sector',
        valueKey: 'label',
        asyncPopulate: function(data) {
            const form = this
            return new Promise((resolve, reject) => {
                if (data) {
                    const currentValue = data.sector
                    if (currentValue !== form.lastValue) {
                        const storeData = Store.state.Miscs
                        const collection = storeData.sector.filter(
                            item => currentValue.indexOf(item.label) !== -1
                        )
                        if (collection.length) {
                            form.lastValue = currentValue
                            const titleMap = collection.reduce((titleMap, item) => {
                                return [...titleMap, ...item.works.map(id => storeData.Works[id])]
                            }, [])
                            resolve({ titleMap: uniqBy(titleMap, 'label') })
                        } else {
                            resolve({ titleMap: [], value: [] })
                        }
                        return
                    }
                }

                reject()
            })
        }
    }, 

    /*{
        key: 'rol',
        type: 'select',
        title: 'ROL',
        attr: {
            placeholder: 'Puedes eligir más de uno',
            mode: 'multiple'
        },
        reactiveKey: 'work',
        valueKey: 'label',
        asyncPopulate: function(data) {
            const form = this
            return new Promise((resolve, reject) => {
                if (data) {
                    const currentValue = data.work
                    if (currentValue !== form.lastValue) {
                        const storeData = Store.state.Miscs
                        const collection = Object.entries(storeData.Works).filter(
                            ([keys, item]) =>
                                currentValue.indexOf(item.label) !== -1 && item.roles.length
                        )
                        if (collection.length) {
                            form.lastValue = currentValue
                            const titleMap = collection.reduce((titleMap, [key, item]) => {
                                return [...titleMap, ...storeData.Roles[item.roles]]
                            }, [])
                            resolve({ titleMap: uniqBy(titleMap, 'label') })
                        } else {
                            resolve({ titleMap: [], value: [] })
                        }
                        return
                    }
                }

                reject()
            })
        }
    },*/

    {
        type: 'separator'
    },

    {
        type: 'help',
        description:
            'Escribe el nombre de la marca, en caso de que no esté en nuestra base de datos la puedes dar de alta.',
        className: 'cell-12 color-white margin-bottom-1'
    },

    'brand',

    {
        key: 'brandLogo',
        type: 'brandAndLogo',
        title: 'Logo de la marca',
        className: 'cell-12 margin-top-2',

        action: data => Store.dispatch('Miscs/UploadFileWork', data),
        attr: {
            accept: 'image/png, image/jpeg',
            listType: 'picture-card'
        }
    },

    {
        type: 'separator'
    },

    {
        key: 'country',
        type: 'select',
        valueKey: 'alpha2Code',
        labelKey: 'name',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                if (Store.state.Miscs.countries.length) {
                    resolve({ titleMap: Store.state.Miscs.countries })
                } else {
                    Store.dispatch('Miscs/GetCountries').then(response =>
                        resolve({ titleMap: response.countries })
                    )
                }
            })
        }
    },

    'city',

    {
        title: 'Facturacion',
        type: 'fieldset',
        className: 'field-set-separator-top cell-12',
        gridClassName: 'padding-x margin--x ',
        fields: ['cache', 'extras']
    },

    {
        type: 'fieldset',
        title: 'Derechos de imagen',
        className: 'field-set-separator-top cell-12',
        gridClassName: 'padding-x margin--x ',
        fields: [
            {
                key: 'copyRight.country',
                type: 'select',
                valueKey: 'alpha2Code',
                labelKey: 'name',
                attr: {
                    placeholder: 'Puedes eligir más de uno',
                    mode: 'multiple'
                },
                asyncPopulate: function(data) {
                    return new Promise((resolve, reject) => {
                        if (Store.state.Miscs.countries.length) {
                            resolve({ titleMap: Store.state.Miscs.countries })
                        } else {
                            Store.dispatch('Miscs/GetCountries').then(response =>
                                resolve({ titleMap: response.countries })
                            )
                        }
                    })
                }
            },
            {
                key: 'copyRight.expiration',
                type: 'dateSelect',
                maxYears: new Date().getFullYear() + 50,
                minYears: new Date().getFullYear()
            }
        ]
    },

    {
        type: 'fieldset',
        title: 'Agencia / reprensentante',
        className: 'field-set-separator-top cell-12',
        gridClassName: 'padding-x margin--x ',
        fields: [
            'manager.name',
            {
                key: 'manager.commission',
                afterValue: '%',
                default: 0,
                attr: {
                    type: 'number'
                }
            }
        ]
    },

    {
        key: 'photos',
        type: 'filesWork',
        title: 'Fotos y Videos',
        className: 'cell-12 margin-top-2',
        minSize: 0.1525,
        maxSize: 800000,
        maxLength: 5,

        action: data => Store.dispatch('Miscs/UploadFileWork', data),
        attr: {
            accept: 'image/png, image/jpeg, video/mp4',
            listType: 'picture-card'
        }
    }
]

// Conotacion del schema
// schema: tipo de propiedad (no object, ni Array)
// key: nombreDelaPropiedad

// schema: propiedad es un object
// key: copyRight.description

// schema Array Country
// key: Country[].name
