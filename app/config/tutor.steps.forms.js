import InfoSchema from 'schemas/tutor';
import InfoForm from './tutor.form';


export default [
    {
        label: 'Info',
        schema: InfoSchema,
        form: InfoForm
    },
];
