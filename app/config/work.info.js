

export default [
    {

        title: 'Facturacion',
        properties: [
            {
                key: 'cache',
                title: 'Caché',
                compute() {
                    return 800
                }
            },

            {
                key: 'extras',
                title: 'Extras',
                compute() {
                    return 800
                }
            },

            {
                key: 'copyRight',
                title: 'Derechos de Imagen',
                compute() {
                    return 400
                }
            },
        ],
    },
    {

        title: 'Derechos de Image',
        properties: [
            {
                key: 'country',
                title: 'Paises',
                compute() {
                    return 'España'
                }
            },

        ],
    },
];