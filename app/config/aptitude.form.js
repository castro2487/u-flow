import Store from 'store';

export default [
    {
        key: 'languages',
        removeLabel: 'Remover idioma',
        buttonLabel: 'Añadir otro idioma',
        titleItem(index) {
            return `Idioma`;
        },
        fields: [
            {
                key: 'languages[].language',
                type: 'select',
                valueKey: 'label',
                asyncPopulate: function(data) {
                    return new Promise((resolve, reject) => {
                        resolve({ titleMap: Store.state.Miscs.Language });
                    });
                }
            },
            {
                key: 'languages[].level',
                type: 'select',
                valueKey: 'label',
                asyncPopulate: function(data) {
                    return new Promise((resolve, reject) => {
                        resolve({ titleMap: Store.state.Miscs.ExpertiseLevel });
                    });
                }
            }
        ]
    },

    {
        key: 'drivingLicense',
        type: 'select',
        className: 'cell-12 margin-top-1',
        minItems: 1,
        attr: {
            placeholder: 'Puedes eligir más de uno',
            mode: 'multiple'
        },
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.DrivingLicense });
            });
        }
    },

    {
        key: 'studies',
        type: 'select',
        minItems: 1,
        attr: {
            placeholder: 'Puedes eligir más de uno',
            mode: 'multiple'
        },
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.Studies });
            });
        }
    },

    {
        key: 'othersStudies',
        type: 'textarea',
        title: 'Otras titulaciones o estudios'
    },

    {
        key: 'specialHabilities',
        type: 'select',
        minItems: 1,
        attr: {
            placeholder: 'Puedes eligir más de uno',
            mode: 'multiple'
        },
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.SpecialHabilities });
            });
        }
    },

    {
        key: 'sports',
        type: 'select',
        minItems: 1,
        attr: {
            placeholder: 'Puedes eligir más de uno',
            mode: 'multiple'
        },
        valueKey: 'label',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                resolve({ titleMap: Store.state.Miscs.Sports });
            });
        }
    }
];
