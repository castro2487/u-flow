import Store from 'store'

export default [
    'razonSocial',
    'name',
    {
        key: 'dni',
        validationMessage: 'Campo inválido >> (000000R)'
    },

    {
        key: 'picture',
        type: 'chooseImage',
        title: 'Elegir foto de perfil',
        className: 'cell-12 margin-top-2',
        minSize: 0.1525,
        maxSize: 20,

        action: data => Store.dispatch('Miscs/UploadFile', data),
        attr: {
            accept: 'image/png, image/jpeg',
            listType: 'picture-card'
        }
    },

    {
        key: 'country',
        type: 'select',
        valueKey: 'alpha2Code',
        labelKey: 'name',
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                //Se verifica que existan paises
                // console.log('COUNTRIES >> ',Store.state.Miscs.countries)
                if (Store.state.Miscs.countries.length) {
                    resolve({ titleMap: Store.state.Miscs.countries })
                } else {
                    // console.log('GetCountries')
                    Store.dispatch('Miscs/GetCountries').then(response =>
                        resolve({ titleMap: response.countries })
                    )
                }
            })
        }
    },

    {
        key: 'province',
        valueKey: 'region',
        labelKey: 'region'
    },

    {
        key: 'city'
    },

    {
        key: 'address'
    },

    {
        key: 'code'
    },

    {
        type: 'fieldset',
        title: 'Teléfono',
        gridClassName: 'padding-x margin--x',
        fields: [
            {
                key: 'areaCode',
                className: 'cell-5',
                type: 'select',
                asyncPopulate: function(data) {
                    return new Promise((resolve, reject) => {
                        if (Store.state.Miscs.areaCodes.length) {
                            resolve({ titleMap: Store.state.Miscs.areaCodes })
                        } else {
                            Store.dispatch('Miscs/GetCountries').then(response =>
                                resolve({ titleMap: response.areaCodes })
                            )
                        }
                    })
                }
            },
            {
                key: 'phone',
                type: 'text',
                className: 'cell',
                attr: {
                    type: 'tel'
                }
            }
        ]
    }
]
