import Store from 'store'
import moment from 'moment'

export default [
    {
        key: 'commission',
        afterValue: '%',
        default: 0,
        attr: {
            type: 'number'
        }
    },

    {
        key: 'startDate',
        type: 'dateSelect',
        default: `${moment().format('YYYY-MM-DD')}T00:00:00`
    },

    {
        key: 'endDate',
        type: 'dateSelect',
        maxYears: new Date().getFullYear() + 50,
        minYears: new Date().getFullYear(),
        default: `${moment()
            .add(1, 'days')
            .format('YYYY-MM-DD')}T00:00:00`
    },

    {
        key: 'country',
        type: 'select',
        valueKey: 'alpha2Code',
        labelKey: 'name',
        attr: {
            placeholder: 'Puedes eligir más de uno',
            mode: 'multiple'
        },
        asyncPopulate: function(data) {
            return new Promise((resolve, reject) => {
                if (Store.state.Miscs.countries.length) {
                    resolve({ titleMap: Store.state.Miscs.countries })
                } else {
                    Store.dispatch('Miscs/GetCountries').then(response =>
                        resolve({ titleMap: response.countries })
                    )
                }
            })
        }
    }
]
