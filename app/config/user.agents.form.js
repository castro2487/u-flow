import Store from 'store'

export default [
    {
        key: 'agents',
        title: 'Representantes/Agencias',
        removeLabel: 'Remover agente/representante',
        buttonLabel: 'Añadir agencia/representante',
        titleItem(index) {
            return 'Representantes/Agencias'
        },
        fields: [
            {
                key: 'agents[].email',
                type: 'select',
                labelKey: 'label',
                title: 'Selecciona tu agencia / representante',
                asyncPopulate: function(data) {
                    return new Promise((resolve, reject) => {
                        resolve({ titleMap: Store.state.Miscs.Agents })
                    })
                }
            },
            {
                type: 'fieldset',
                title: 'Invita a tu agencia / representante ',
                gridClassName: 'padding-x margin--x',
                fields: [
                    {
                        key: 'agents[].guest',
                        title: 'Email'
                    }
                ]
            }
        ]
    }
]
