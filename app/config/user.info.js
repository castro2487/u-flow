import Store from 'store'
import { getYearsOld } from 'utils/DateUtils'
export default [
    {
        title: 'Información basica',
        icon: 'Building',
        properties: [
            {
                key: 'name',
                title: 'Nombre'
            },

            {
                key: 'lastName',
                title: 'Apellido'
            },

            {
                key: 'sndLastName',
                title: 'Segundo apellido'
            },

            {
                key: 'dni',
                title: 'DNI'
            },

            {
                key: 'nickName',
                title: 'Apodo'
            },

            {
                key: 'bio',
                title: 'Bio'
            },

            {
                key: 'addresses',
                type: 'array',
                title: 'Direcciones',
                compute(model, value, index) {
                    const data = value[index]
                    return `${data.address || ''} ${data.code || ''} ${data.city ||
                        ''} ${data.province || ''}`
                },
                computeTitle(model, value, index) {
                    return `Localización ${index ? 'alernativa' : 'principal'}`
                }
            },

            {
                key: 'decorator',
                title: 'Teléfono',
                compute(model) {
                    return model.phone ? `${model.areaCode} ${model.phone || ''}` : ''
                }
            },

            {
                key: 'gender',
                title: 'Género',
                compute(model, value) {
                    return Store.getters['Miscs/gender'](value, '')
                }
            },

            {
                key: 'birthday',
                title: 'Edad',
                compute(model, value = '') {
                    try {
                        const age = getYearsOld(value)
                        return `${age} años`
                    } catch (e) {
                        return ''
                    }
                }
            },

            {
                key: 'sector',
                type: 'list',
                title: 'Sector'
            },

            {
                key: 'work',
                type: 'list',
                title: 'Tipos de trabajo'
            }

            /* {
                key: 'rol',
                type: 'list',
                title: 'Rol'
            }*/
        ]
    },
    {
        title: 'Educación',
        icon: 'Book',
        properties: [
            {
                title: 'Nivel de estudios',
                compute(model, value) {
                    return (model.studies.degree || {}).title || '...'
                }
            },

            {
                title: '¿Planeas seguir estudiando?',
                compute(model, value) {
                    return (model.studies.degree || {}).continueStudying || '...'
                }
            },

            {
                title: 'Idioma',
                key: 'studies.languages',
                type: 'array',
                compute(model, value, index) {
                    const data = value[index]
                    return `${data.language || ''}`
                },
                computeTitle(model, value, index) {
                    return `Idioma ${ value[index].level}`
                }
            }
        ]
    },
    {
        title: 'Comportamineto en el campo',
        icon: 'Running',
        title: 'behavior.title',
        type: 'behavior',
        properties: {
            key: 'behaviorPosition',

            position: {
                key: 'position',
                title: 'position.position'
            },

            questions: [
                {
                    key: 'fieldAreaHighlight',
                    title: 'behavior.field_area_highlight',
                    video: 'attachment',
                    zone: 'fieldZone'
                },

                {
                    key: 'fieldAreaSpendMoreTime',
                    title: 'behavior.field_area_spend_more_time',
                    video: 'attachment',
                    zone: 'fieldZone'
                },

                {
                    key: 'fieldAreaLikeToSpendMoreTime',
                    title: 'behavior.field_area_like_to_spend_more_time',
                    video: 'attachment',
                    zone: 'fieldZone'
                },

                {
                    key: 'fieldAreaMostReceiveTheBall',
                    title: 'behavior.field_area_most_receive_the_ball',
                    video: 'attachment',
                    zone: 'fieldZone'
                },

                {
                    key: 'fieldAreaMostRecoverTheBall',
                    title: 'behavior.field_area_most_recover_the_ball',
                    video: 'attachment',
                    zone: 'fieldZone'
                },

                {
                    key: 'fieldAreaMostHittingTheBall',
                    title: 'behavior.field_area_most_hitting_the_ball',
                    video: 'attachment',
                    zone: 'fieldZone'
                },

                {
                    title: 'behavior.general',
                    zone: 'all'
                }
            ]
        }
    },
    {
        title: 'Habilidades',
        icon: 'Cycling',
        type: 'skills',
        properties: [
            {
                title: 'skills.main_skills',
                key: 'mainSkills',
                color: '#455E34',
                properties: [
                    {
                        key: 'shooting',
                        title: 'skills.shooting',
                        description: 'skills.shooting_description'
                    },
                    {
                        key: 'head',
                        title: 'skills.head',
                        description: 'skills.head_description'
                    },
                    {
                        key: 'strategy',
                        title: 'skills.strategy',
                        description: 'skills.strategy_description'
                    },
                    {
                        key: 'passing',
                        title: 'skills.passing',
                        description: 'skills.passing_description'
                    },
                    {
                        key: 'lateralPlay',
                        title: 'skills.lateral_play',
                        description: 'texto skills.lateral_play_description'
                    },
                    {
                        key: 'finishing',
                        title: 'skills.finishing',
                        description: 'texto skills.finishing_description'
                    },
                    {
                        key: 'leftLeg',
                        title: 'skills.left_leg',
                        description: 'skills.left_leg_description'
                    },
                    {
                        key: 'rightLeg',
                        title: 'skills.right_leg',
                        description: 'texto skills.right_leg_description'
                    },
                    {
                        key: 'control',
                        title: 'skills.control',
                        description: 'skills.control_description'
                    }
                ]
            },
            {
                title: 'skills.tactic_skills',
                key: 'tacticSkills',
                color: '#5F8F3E',
                properties: [
                    {
                        key: 'vision',
                        title: 'skills.vision',
                        description: 'skills.vision_description'
                    },
                    {
                        key: 'awareness',
                        title: 'skills.awareness',
                        description: 'skills.awareness_description'
                    },
                    {
                        key: 'ballMoviment',
                        title: 'skills.ball_moviment',
                        description: 'skills.ball_moviment_description'
                    }
                ]
            },
            {
                title: 'skills.character_skill',
                key: 'character',
                color: '#7CB754',
                properties: [
                    {
                        key: 'recovery',
                        title: 'skills.recovery',
                        description: 'skills.recovery_description'
                    },
                    {
                        key: 'physicalBravery',
                        title: 'skills.physical_bravery',
                        description: 'skills.physical_bravery_description'
                    },
                    {
                        key: 'mentalBravery',
                        title: 'skills.mental_bravery',
                        description: 'skills.mental_bravery_description'
                    },
                    {
                        key: 'energy',
                        title: 'skills.energy',
                        description: 'skills.energy_description'
                    },
                    {
                        key: 'leadership',
                        title: 'skills.leadership',
                        description: 'skills.leadership_description'
                    },
                    {
                        key: 'communication',
                        title: 'skills.communication',
                        description: 'skills.communication_description'
                    }
                ]
            },
            {
                title: 'skills.athleticSkills',
                key: 'athleticSkills',
                color: '#112404',
                properties: [
                    {
                        key: 'speed',
                        title: 'skills.speed',
                        description: 'skills.speed_description'
                    },
                    {
                        key: 'acuity',
                        title: 'skills.acuity',
                        description: 'skills.acuity_description'
                    },
                    {
                        key: 'mobility',
                        title: 'skills.mobility',
                        description: 'skills.mobility_description'
                    },
                    {
                        key: 'bodyStrength',
                        title: 'skills.body_strength',
                        description: 'skills.body_strength_description'
                    },
                    {
                        key: 'rhythm',
                        title: 'skills.rhythm',
                        description: 'skills.rhythm_description'
                    }
                ]
            }
        ]
    },
    {
        title: 'Temporadas',
        icon: 'Schedule',
        properties: [
            {
                title: 'sessons',
                key: 'seasonList',
                properties: [
                    {
                        key: 'totalTeamMatchs',
                        title: 'season.team_matchs'
                    },
                    {
                        key: 'totalPlayerMatchs',
                        title: 'season.games_played'
                    },
                    {
                        key: 'minutesPlayed',
                        title: 'season.minutes_played'
                    },
                    {
                        key: 'totalGoals',
                        title: 'season.goles'
                    },
                    {
                        key: 'totalKeyPasses',
                        title: 'season.passings'
                    },
                    {
                        key: 'totalYellowCards',
                        title: 'season.yellow_cards'
                    },
                    {
                        key: 'totalRedCards',
                        title: 'season.red_cards'
                    }
                ]
            }
        ]
    }
]
