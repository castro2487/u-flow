export default [
    {
        key: 'contacts',
        title: 'Persona de contacto',
        removeLabel: 'Remover persona de contacto',
        buttonLabel: 'Añadir persona de contacto',
        titleItem(index) {
            return 'Persona de contacto'
        },
        fields: [
            {
                key: 'contacts[].name'
            },
            {
                key: 'contacts[].lastName'
            },

            {
                key: 'contacts[].cargo'
            },
            
            {
                key: 'contacts[].email'
            },

            {
                type: 'fieldset',
                title: 'Teléfono',
                gridClassName: 'padding-x margin--x',
                fields: [
                    {
                        key: 'contacts[].areaCode',
                        className: 'cell-5',
                        type: 'select',
                        asyncPopulate: function(data) {
                            return new Promise((resolve, reject) => {
                                if (Store.state.Miscs.areaCodes.length) {
                                    resolve({ titleMap: Store.state.Miscs.areaCodes })
                                } else {
                                    Store.dispatch('Miscs/GetCountries').then(response =>
                                        resolve({ titleMap: response.areaCodes })
                                    )
                                }
                            })
                        }
                    },
                    {
                        key: 'contacts[].phone',
                        type: 'text',
                        className: 'cell',
                        attr: {
                            type: 'tel'
                        }
                    }
                ]
            },

            {
                key: 'contacts[].birthday',
                title: 'Fecha de nacimiento',
                type: 'dateSelect'
            }
        ]
    }
]
