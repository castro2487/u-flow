import { uniqBy } from 'lodash/array'
import { getApiUrl } from 'utils/ClientServices'
import { range, hasLegalAge } from 'utils/helpers'
import Store from 'store'

export default [
    {
        key: 'experiences',
        removeLabel: 'Remover experiencia',
        buttonLabel: 'Añadir otra experiencia',
        title: 'Experiencia',

        fields: [
            {
                key: 'experiences[].season',
                type: 'seasonSelector',
                title: 'Temporada',
            },
            {
                key: 'experiences[].clubName',
                type: 'string',
                title: 'Club',
            },
            {
                key: 'experiences[].clubLogo',
                type: 'brandAndLogo',
                title: 'Añadir logo del Club',
                className: 'cell-12 margin-top-2',
        
                action: data => Store.dispatch('Miscs/UploadFileWork', data),
                attr: {
                    accept: 'image/png, image/jpeg',
                    listType: 'picture-card'
                }
            },
            {
                key: 'experiences[].totalTeamMatchs',
                type: 'number',
                title: 'Partidos jugados por el equipo',
            },
            {
                key: 'experiences[].totalPlayerMatchs',
                type: 'number',
                title: 'Partidos jugados del jugador',
            },
            {
                key: 'experiences[].minutesPlayed',
                type: 'number',
                title: 'Minutos de juego',
            },
            {
                key: 'experiences[].totalGoals',
                type: 'number',
                title: 'Goles',
            },
            {
                key: 'experiences[].totalKeyPasses',
                type: 'number',
                title: 'Pases decisivos',
            },
            {
                key: 'experiences[].totalYellowCards',
                type: 'number',
                title: 'Tarjetas amarillas',
            },
            {
                key: 'experiences[].totalRedCards',
                type: 'number',
                title: 'Tarjetas rojas',
            }
            ,{
                key: 'experiences[].ranking',
                type: 'number',
                title: 'Puesto',
            }
            ,{
                key: 'experiences[].trainner',
                type: 'string',
                title: 'Entrenador',
            },
        ]
    }
]