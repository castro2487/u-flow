import InfoSchema from 'schemas/personalPlayoneer'
import InfoForm from './personalPlayoneer.form'

import socialSchema from 'schemas/social'
import socialForm from './social.form'

import studiesSchema from 'schemas/studies'
import studiesForm from './profile.studies.form'

import skillsSchema from 'schemas/skills'
import skillsForm from './skills.form'

import behaviorSchema from 'schemas/behavior'
import behaviorForm from './behavior.form'

import seasonsSchema from 'schemas/seasons'
import seasonsForm from './seasons.form'

export default [
    {
        label: 'Información basica',
        icon: 'Building',
        schema: InfoSchema,
        form: InfoForm
    },
    {
        label: 'Educación',
        icon: 'Book',
        schema: studiesSchema,
        form: studiesForm
    },
    {
        label: 'Comportamineto en el campo',
        icon: 'Running',
        schema: behaviorSchema,
        form: behaviorForm,
        
    },
    {
        label: 'Habilidades',
        icon: 'Cycling',
        description: 'skill_description',
        schema: skillsSchema,
        form: skillsForm
    },
    {
        label: 'Temporadas',
        icon: 'Running',
        schema: seasonsSchema,
        form: seasonsForm
    },
]
