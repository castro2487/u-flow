export default [
    {
        title: 'Información básica',
        properties: [
            {
                key: 'name',
                title: 'Nombre'
            },

            {
                key: 'lastName',
                title: 'Apellido'
            },

            {
                key: 'dni',
                title: 'DNI'
            },

            {
                key: 'decorator',
                title: 'Teléfono',
                compute(model) {
                    return model.phone ? `${model.areaCode} ${model.phone || ''}` : ''
                }
            },

        ]
    },
   
]
