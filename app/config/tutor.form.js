import { uniqBy } from 'lodash/array'
import { getApiUrl } from 'utils/ClientServices'
import Store from 'store'
import { hasLegalAge } from 'utils/helpers'

export default [
  

    { key: 'name', default: '...' },
    { key: 'lastName', default: '...' },
    {
        key: 'dni',
        default: '...',
        validationMessage: 'Campo inválido >> (000000R)'
    },
    { key: 'email', default: '...' },
    {
        type: 'fieldset',
        title: 'Teléfono',
        gridClassName: 'padding-x margin--x',
        fields: [
            {
                key: 'areaCode',
                className: 'cell-5',
                type: 'select',
                asyncPopulate: function(data) {
                    return new Promise((resolve, reject) => {
                        if (Store.state.Miscs.areaCodes.length) {
                            resolve({ titleMap: Store.state.Miscs.areaCodes })
                        } else {
                            Store.dispatch('Miscs/GetCountries').then(response =>
                                resolve({ titleMap: response.areaCodes })
                            )
                        }
                    })
                }
            },
            {
                key: 'phone',
                type: 'text',
                default: '...',
                className: 'cell',
                attr: {
                    type: 'tel'
                }
            }
        ]
    },

    {
        key: 'picture',
        type: 'chooseImage',
        title: 'Elegir foto de perfil',
        className: 'cell-12 margin-top-2',
        minSize: 0.1525,
        maxSize: 20,

        action: data => Store.dispatch('Miscs/UploadFile', data),
        attr: {
            accept: 'image/png, image/jpeg',
            listType: 'picture-card'
        }
    },
   
]
