export const ROOT = ROOT_PATH;
export const HOST = process.env.NODE_ENV === "development" ? 'http://localhost:9003' : API_URI;
export const SERVICE_URI = API_URI;
export const MASTER_KEY  = 'zIi5f2XoJ4cWLaZIVYhP3YRUbueDJbcq';
export const debug = process.env.NODE_ENV !== 'production';
