import utils from './formUtils';

export function runValidation(form, value, multi){
    const validationResult = utils.validate(form, value, multi);
    return getValidationMessage(form, validationResult);
}

function getValidationMessage( form, validationResult ) {
    if ( !validationResult.valid ) {
        if (validationResult.error) {
            validationResult.message = getMessage(form, validationResult.error);
            return validationResult;
        }
        else {
            let errors = getMultipleMessage(form, validationResult.errors);
            return {
                error: validationResult.errors,
                missing: validationResult.missing,
                toString: validationResult.toString,
                valid: false
            }
        }
    }
    return validationResult;
}

function getMessage(form, error) {

    return formatValidation(
        ( form && form.validationMessage && (form.validationMessage[error.code] || form.validationMessage.default ))
           || error.message 
           || 'Unknow error', form, error);
}

function formatValidation(message, form, error) {
    if ( form ) {
        let validator = form.validatorMessage;
        if (typeof validator === 'function') {
            message = validator( parseMessage( message, error ), error );
        }
        else {
            message = parseMessage( message, error );
        }
    }
    return message; 
}

function parseMessage( message, error ) {
    let params = error.params;
    let prop = error.dataPath.substr(error.dataPath.lastIndexOf('/') + 1);
    return message.replace(/\{([^{}]*)\}/g, function (whole, varName) {
        let subValue = params[varName];
        return typeof subValue === 'string' || typeof subValue === 'number' ? subValue : whole;
    }).replace('{key}',prop);
}

function getMultipleMessage(form, errors) {
    errors.message = '';
    errors.map((error)=>{
        let prop = error.dataPath.substr(error.dataPath.lastIndexOf('/') + 1);
        let code = error.dataPath.replace(/\//g,'.')
                                 .replace(/\.(\d)/, (replacement, captured) => `[${captured}]`)
                                 .substr(1);
        error.message = getMessage(form.schema.items.properties[prop], error);
        errors.message += error.message  + ' ';
    });
    return errors;
}