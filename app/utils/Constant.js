
const _index = [];

export function createConstants(...constants) {
    return constants.reduce((acc, constant) => {
        acc[constant] = _index.push(constant);
        return acc;
    }, {});
}
