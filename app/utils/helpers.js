export function handleErrors(response) {
    if (!response.ok) {
        return response
            .text()
            .then(text => (text ? JSON.parse(text) : { ok: false }))
            .then(json => Promise.reject(json || {}))
    }
    return response.status !== 204 ? response.json() : null
}

export function append(content, values) {
    Object.keys(values).forEach(key => content.append(key, values[key]))

    return content
}

// Modifica intercambia la  posicion del un item dentro de un array
export const addToPosition = (arr, dragResult) => {
    const { removedIndex, addedIndex, payload } = dragResult

    if (removedIndex === null && addedIndex === null) return arr

    const result = [...arr]
    let itemToAdd = payload

    if (removedIndex !== null) {
        itemToAdd = result.splice(removedIndex, 1)[0]
    }

    if (addedIndex !== null) {
        result.splice(addedIndex, 0, itemToAdd)
    }

    return result
}

// Devuelve un array numerico, partiendo de un numero minimo a un maximo.
export const range = (since, until, per=1) => {
    const collection = []
    for (; since <= until; since += per) {
        collection.push(since < 10 ? `0${since}` : `${since}`)
    }

    return collection
}

// Devuelve los años segun la fecha de nacimiento
export const getAge = date => {
    var ageDifMs = Date.now() - new Date(date).getTime()
    var ageDate = new Date(ageDifMs)
    return Math.abs(ageDate.getUTCFullYear() - 1970)
}

// Verifica que el usuario sea mayoy a 18
// TODO: tendria que verificarse a nivel país cual es la edad legal.
export const hasLegalAge = date => {
    if (date) {
        return getAge(date) >= 18
    } else {
        return false
    }
}

// Devuelve un numero randomico entre un rango
export function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}

export function plainObject(data) {
    return JSON.parse(JSON.stringify(data))
}
