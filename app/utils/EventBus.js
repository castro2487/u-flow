import Vue from 'vue';

const Events = {};

const Bus = (name) => {
    if (!(name in Events)) {
        const bus = new Vue;
        Events[name] = {
            bus,

            subscribe(name, callback) {
                bus.$on(name, callback);
            },

            emit(event, args) {
                bus.$emit(event, args);
            },

            unsubscribe(name, callback) {
                bus.$off(name, callback);
            },

            once(name, callback) {
                bus.$once(name, callback);
            },

        }
    }

    return Events[name];
}


export default Bus;