const Cookies = {
    storeDate: '',

    encodeChar(c) {
        return '%' + c.charCodeAt(0).toString(16);
    },

    encodeASCII(str) {
        return encodeURIComponent(str).replace(/[-_z.]/g, Cookies.encodeChar).replace(/%/g, 'z');
    },

    decodeASCII(str) {
        return decodeURIComponent(str.replace(/z/g, '%'));
    },

    setStoreDate(storeDate) {
        Cookies.storeDate = storeDate;
    },

    setCookie(name, value, date, domain) {
        const valueStr = value.toGMTString ? value.toGMTString().replace(',', '') : value + '';
        let buffer = name + '=' + Cookies.encodeASCII(valueStr);

        if (date) {
            const dateStr = date.toGMTString ? date.toGMTString(): date + '';

            if (Cookies.storeDate)
                document.cookie = name + 'Date=' + dateStr + ';expires=' + dateStr + ';path=/';
            buffer += ';expires=' + dateStr + ';path=/';

            if (domain) {
                buffer += ';domain='+domain;
            }

        }
        // console.log(buffer)
        document.cookie = buffer;
    },

    getCookie(name) {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].split('=');
            if ((cookie[0]).replace(/^\s*|\s*$/g, '') == name) {
                // console.log('111',Cookies.decodeASCII(cookie[1]))
                return Cookies.decodeASCII(cookie[1]);
            }
        }
    },

    updateCookie(name, value, domain) {
        const date = Cookies.getCookie(name + 'Date');
        Cookies.setCookie(name, value, date, domain);
    },
}

export default Cookies;