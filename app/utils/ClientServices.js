import { SERVICE_URI, MASTER_KEY } from 'config/_constants'
import { handleErrors, append, plainObject } from './helpers'

export const getApiUrl = url => `${SERVICE_URI}/${url}`

export const Get = url => fetch(url, { method: 'GET' }).then(handleErrors)

export const Post = (url, data) => fetch(url, { method: 'POST', ...data }).then(handleErrors)

export const Put = (url, data) => fetch(url, { method: 'PUT', ...data }).then(handleErrors)

export const Delete = (url, data) => fetch(url, { method: 'DELETE', ...data }).then(handleErrors)

export default {
    get(url, data) {
        const token = data
            ? append(new URLSearchParams(), {
                  ...data
              }).toString()
            : ''

        return Get(`${SERVICE_URI}/${url}?${token}`)
    },

    post(url, data = {}, useMasterKey=false) {
        let body = plainObject(data)
        
        if (useMasterKey) {
            data.access_token = MASTER_KEY
        } 
        
        return Post(`${SERVICE_URI}/${url}`, {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(plainObject(data))
        })
    },

    upload(url, data = {}, headers = {}) {
        const body = append(new FormData(), {
            ...data
        })
        return Post(`${SERVICE_URI}/${url}`, {
            headers,
            body
        })
    },

    put(url, data = {}) {
        return Put(`${SERVICE_URI}/${url}`, {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ ...data })
        })
    },

    delete(url, data = {}) {
        const body = append(new URLSearchParams(), {
            ...data
        })

        return Delete(`${SERVICE_URI}/${url}`, { body })
    },

    auth(url, data) {
        
        
        const headers = new Headers()
        headers.append('Authorization', `Basic ${btoa(`${data.email}:${data.password}`)}`)
        
        
        
        let body = new URLSearchParams()
        body.append('access_token', MASTER_KEY)
        body.append('role', data.role)

        return fetch(`${SERVICE_URI}/${url}`, { method: 'POST', headers, body }).then(handleErrors)
    }
}
