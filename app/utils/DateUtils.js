export const getYearsOld = (date) => {
    const birthday = new Date(date);
    const ageDifMs = Date.now() - birthday.getTime();
    const ageDate = new Date(ageDifMs); 
    const age = Math.abs(ageDate.getUTCFullYear() - 1970);
    return `${isNaN(birthday) ? '' : age }`
}