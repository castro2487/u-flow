export const STATUS = {
    FAILD: 'faild',
    SUCCESS: 'success',
    FETCHING: 'fetching',
}


export const StatusError = (commit, info={}, code=400, extraData={}) => {
    const data = {
        ...extraData,
        status: { type: STATUS.FAILD, info, code }
    }
    commit('SetData', data)
    return data;
}

export const StatusFetching = (commit, info={}, code= 0, extraData={}) => {
    const data = {
        ...extraData,
        status: { type: STATUS.FETCHING, info, code }
    }
    commit('SetData', {
        extraData,
        status: { type: STATUS.FETCHING, info, code }
    });
    return data;
}

export const StatusSuccess = (commit, info={}, code= 200, extraData={}) => {
    const data =  {
        ...extraData,
        status: { type: STATUS.SUCCESS, info, code }
    };
    commit('SetData', data);
    return data;
}

export const DataMutation = {
    SetData(state, data) {
        Object.entries(data)
              .forEach(([key, field]) => {
                state[key] = field;
              })
    },
}