import Vue from 'vue'
import VueI18n from 'vue-i18n'
import moment from 'moment';
import { handleErrors, append } from './helpers';
import tv4 from 'tv4';
import Cookies from './Cookies';

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'es',
});


export default i18n;

const loadedLanguages = {} // our default language that is preloaded 


function setI18nLanguage (lang) {
  i18n.locale = lang;
  tv4.language(lang);
  moment.locale(lang)
  Cookies.setCookie('AppLang', lang);
  document.querySelector('html').setAttribute('lang', lang);
  return lang;
}

// Set section locale
export function LoadSectionLocale(section) {
  const lang = i18n.locale;
  if (loadedLanguages[lang].indexOf(section) === -1 && section != 'global') {
    return fetch(`/locale/${section}/${lang}.json`, { method: 'GET' })
            .then(handleErrors)
            .then(msgs => {
                // Save section locale data
                i18n.mergeLocaleMessage(lang, {[section]: msgs});
                // Save section name load for update Language;
                loadedLanguages[lang].push(section);
            })
  } 
  return Promise.resolve(section);
}

// Set global locale
export function LoadLocale(lang, section='global') {
  if (i18n.locale !== lang ) {
    if (!(lang in loadedLanguages)) {
      return fetch(`/locale/${lang}.json`)
              .then(handleErrors)
              .then(msgs => {
                  // Setting data in i18n 
                  i18n.setLocaleMessage(lang, msgs);
                  tv4.addLanguage(lang, msgs.errors)
                  // Register last lang loaded
                  const current = loadedLanguages[lang] = [section];
                  // Change lng of html for accessibility
                  setI18nLanguage(lang);
                  // Refresh section locale
                  return LoadSectionLocale(section);
              })
    } 
    return Promise.resolve(setI18nLanguage(lang));
  }
  return Promise.resolve(lang);
}