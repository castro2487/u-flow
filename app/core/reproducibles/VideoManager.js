import Emitter from 'component-emitter'

import MediaEvents from './MediaEvents'
import PlayerHTML from './PlayerHTML'
import PlayerYoutube from './PlayerYoutube'
import PlayerVimeo from './PlayerVimeo'

const VIDEO_MAPPER = [PlayerYoutube, PlayerVimeo, PlayerHTML]
const PLAYERS = {}

// TODO: Falta mejorar la api de la Third parties


const DefaultPlayer = {
    play() {},

    replay() {},

    pause() {},

    mute() {},

    unmute() {},

    seekTo(seconds) {},

    load() {},

    setSrc(src) {},

    stop() {},
    
    setVolume(vol) {},

    getVolume() {
        return 0
    },

    getMuted() {
        return false
    },

    getCurrentTime() {
        return 0
    },

    getDuration() {
        return 0
    },

    getBuffered() {
        return 0
    },

    getSource() {
        return ''
    },

    getVideoUrl() {
        return ''
    },

    isBuffering() {
        return false
    },

    isPaused() {
        return false
    }
}

const Manager = {
    create(options = {}) {
        const controller = VIDEO_MAPPER.find(item => item.canPlay(options.url))

        this.stopAll()

        if (controller.canCreate && controller.canCreate()) {
            Manager.setup(controller, options)
        } else if (controller.loadAPI) {
            controller.loadAPI(() => Manager.setup(controller, options))
        } else {
            console.warn('Manager >> create >> The Controller does not comply with the interface')
        }
    },

    setup(controller, options = {}) {
        if (PLAYERS[options.id]) {
            PLAYERS[options.id].destroy()
            delete PLAYERS[options.id]

            try {
                document.getElementById(options.id).innerHTML = ''
            } catch (e) {}
        }

        if (controller.create) {
            const current = (PLAYERS[options.id || 'default'] = Emitter(controller.create(options)))
            return current
        }

        return null
    },

    getById(id) {
        return PLAYERS[id] || DefaultPlayer
    },

    stopAll() {
        try {
            Object.values(PLAYERS).forEach(player => player.stop())
        } catch (e) {
            console.log('Manager >> StopAll >>', e)
        }
    },

    destroy(id) {
        try {
            console.log('destroy', id)
            PLAYERS[id].pause()
            PLAYERS[id].destroy()
        } catch (e) {
            console.log(`VideoManager >> destroy >> ${id}`, e)
        }
    }
}

export default Manager
