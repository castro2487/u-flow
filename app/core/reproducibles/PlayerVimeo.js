import Emitter from 'component-emitter'
import MediaEvents from 'vue'

class VimeoPlayer {
    constructor({ url, ...playerVars }) {
        this.videoId = url
        this.playerVars = playerVars
        this.element = document.createElement('div')

        this.dispatcher = new Emitter()
        this.setup()
    }

    setup() {
        const { id, ...options } = this.playerVars
        this.player = new window.Vimeo.Player(id, {
            url: this.videoId,
            ...options
        })
        this.onReady()
    }

    canPlayType() {
        return true
    }

    onReady() {
        this.ready = true

        this.player.on('play', () => this.dispatcher.emit('stateChange', 'play'))
        this.player.on('pause', () => this.dispatcher.emit('stateChange', 'pause'))
        this.player.on('ended', () => this.dispatcher.emit('stateChange', 'ended'))
        this.player.on('timeupdate', () => this.dispatcher.emit('stateChange', 'timeupdate'))
        this.player.on('progress', () => this.dispatcher.emit('stateChange', 'progress'))
        this.player.on('seeking', () => this.dispatcher.emit('stateChange', 'seeking'))
        this.player.on('seeked', () => this.dispatcher.emit('stateChange', 'seeked'))
        this.player.on('volumechange', () => this.dispatcher.emit('stateChange', 'volumechange'))
        this.player.on('texttrackchange', () =>
            this.dispatcher.emit('stateChange', 'texttrackchange')
        )
        this.player.on('cuechange', () => this.dispatcher.emit('stateChange', 'cuechange'))
        this.player.on('error', () => this.dispatcher.emit('stateChange', 'error'))
        this.player.on('loaded', () => this.dispatcher.emit('stateChange', 'loaded'))
        this.player.on('cuepoint', () => this.dispatcher.emit('stateChange', 'cuepoint'))
        this.player.on('playbackratechange', () =>
            this.dispatcher.emit('stateChange', 'playbackratechange')
        )
        this.player.on('bufferstart', () => this.dispatcher.emit('stateChange', 'bufferstart'))
        this.player.on('bufferend', () => this.dispatcher.emit('stateChange', 'bufferend'))
        this.dispatcher.emit('ready')
    }

    onStateChange(event) {}

    /**
     *
     */
    play() {
        this.player.play()
    }

    /**
     *
     */
    replay() {
        this.player.play()
    }

    /**
     *
     */
    pause() {
        this.player.pause()
    }

    /**
     *
     * @param mute
     */
    mute() {
        this.setVolume(0)
    }

    unmute() {
        this.setVolume(this._currentVolumen)
    }

    /**
     *
     * @param seconds
     * @param disableSeekAhead
     */
    seekTo(seconds, disableSeekAhead = false) {
        this.player.setCurrentTime(seconds, !disableSeekAhead)
    }

    /**
     *
     */
    load() {}

    /**
     *
     * @param src
     */
    setSrc(src) {
        this.player.loadVideoById({ videoId: src, startSeconds: 0 })
    }

    /**
     *
     * @param vol
     */
    setVolume(vol) {
        vol = vol <= 1 ? vol * 100 : vol
        if (vol > 20) this._currentVolumen = vol
        this.player.setVolume(vol)
    }

    /**
     *
     * @returns {*}
     */
    getVolume() {
        return this.player.getVolume
            ? player.getVolume().then(function(volume) {
                  return volume
              })
            : 0
    }

    /**
     *
     * @returns {any}
     */
    getMuted() {
        return this.getVolume() == 0
    }

    /**
     *
     * @returns {*}
     */
    getCurrentTime() {
        return this.player.getCurrentTime ? this.player.getCurrentTime() : 0
    }

    /**
     *
     * @returns {any|number}
     */
    getDuration() {
        return this.player.getDuration ? this.player.getDuration() : null
    }

    /**
     *
     */
    getBuffered() {
        var percent = (this.player.getVideoBytesLoaded() / this.player.getVideoBytesTotal()) * 100
        return (this.getDuration() * percent) / 100
    }

    /**
     *
     * @returns {any|string}
     */
    getSource() {
        return this.player.getVideoUrl()
    }

    getVideoUrl() {
        return this.player.getVideoUrl()
    }

    isBuffering() {
        return false
    }

    /**
     *
     * @returns {boolean}
     */
    isPaused() {
        return this.player.getPlayerState() != 1
    }

    stop() {
        this.pause()
        this.setCurrentTime(0)
    }

    destroy() {
        this.player.off('play')
        this.player.off('pause')
        this.player.off('ended')
        this.player.off('timeupdate')
        this.player.off('progress')
        this.player.off('seeking')
        this.player.off('seeked')
        this.player.off('volumechange')
        this.player.off('texttrackchange')
        this.player.off('cuechange')
        this.player.off('error')
        this.player.off('loaded')
        this.player.off('cuepoint')
        this.player.off('playbackratechange')
        this.player.off('bufferstart')
        this.player.off('bufferend')
        this.player.destroy()
    }
}

let delay = null

export default {
    canPlay(url) {
        return /vimeo/.test(url)
    },

    canCreate() {
        return window.Vimeo
    },

    create(options) {
        return new VimeoPlayer(options)
    },

    loadAPI(callback) {
        if (delay) return

        const apiUrl = '//player.vimeo.com/api/player.js'
        const scriptTags = document.getElementsByTagName('script')
        let i = scriptTags.length
        let scriptTag

        while (i) {
            i -= 1
            if (scriptTags[i].src === apiUrl) {
                scriptTag = scriptTags[i]
                break
            }
        }

        if (!scriptTag) {
            scriptTag = document.createElement('script')
            scriptTag.src = apiUrl
        }
        scriptTag = document.createElement('script')
        scriptTag.src = apiUrl
        scriptTag.onLoad = callback
        scriptTags[0].parentNode.insertBefore(scriptTag, scriptTags[0])

        delay = setInterval( () => {
            if (this.canCreate()) {
                callback()
                clearInterval(delay)
            }
        },100)
    }
}
