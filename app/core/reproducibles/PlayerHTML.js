import Emitter from 'component-emitter'

const css = (element, styles) => {
    Object.entries(styles).forEach(([key, value]) => {
        element.style[key] = value
    })
}

class VideoPlayer {
    constructor({ url, playerId, ...playerVars }) {
        this.videoId = playerId || url
        this.playerVars = playerVars
        this.element = document.createElement('video')
        this.parent = document.getElementById(playerVars.id)
        this.onStateChange = this.onStateChange.bind(this)
        this.dispatcher = Emitter(this)
        this.setup()
    }

    setup() {
        this.onReady()
    }

    canPlayType() {
        return true
    }

    onReady() {
        this.ready = true
        if (this.parent) {
            this.element.src = this.videoId
            this.element.controls = true
            this.element.setAttribute('width',  '100%')
            this.element.setAttribute('height',  '100%')

            if (this.playerVars.style) {
                css(this.element, this.playerVars.style)
            }

            this.player = this.element
            this.player.addEventListener('abort', this.onStateChange)
            this.player.addEventListener('canplay', this.onStateChange)
            this.player.addEventListener('canplaythrough', this.onStateChange)
            this.player.addEventListener('durationchange', this.onStateChange)
            this.player.addEventListener('emptied', this.onStateChange)
            this.player.addEventListener('ended', this.onStateChange)
            this.player.addEventListener('error', this.onStateChange)
            this.player.addEventListener('loadeddata', this.onStateChange)
            this.player.addEventListener('loadedmetadata', this.onStateChange)
            this.player.addEventListener('loadstart', this.onStateChange)
            this.player.addEventListener('pause', this.onStateChange)
            this.player.addEventListener('play', this.onStateChange)
            this.player.addEventListener('progress', this.onStateChange)
            this.player.addEventListener('seeking', this.onStateChange)
            this.player.addEventListener('suspend', this.onStateChange)
            this.player.addEventListener('timeupdate', this.onStateChange)
            this.player.addEventListener('volumechange', this.onStateChange)
            this.player.addEventListener('waiting', this.onStateChange)
            this.parent.appendChild(this.element)
            this.dispatcher.emit('ready')
        }
    }

    onStateChange(event) {
        this.dispatcher.emit('stateChange', event.type)
    }

    /**
     *
     */
    play() {
        this.player.playVideo()
    }

    /**
     *
     */
    replay() {
        this.player.playVideo()
    }

    /**
     *
     */
    pause() {
        this.player.pauseVideo()
    }

    /**
     *
     * @param mute
     */
    mute() {
        this.setVolume(0)
    }

    unmute() {
        this.setVolume(this._currentVolumen)
    }

    /**
     *
     * @param seconds
     * @param disableSeekAhead
     */
    seekTo(seconds, disableSeekAhead = false) {
        this.player.seekTo(seconds, !disableSeekAhead)
    }

    /**
     *
     */
    load() {}

    /**
     *
     * @param src
     */
    setSrc(src) {
        this.player.loadVideoById({ videoId: src, startSeconds: 0 })
    }

    /**
     *
     * @param vol
     */
    setVolume(vol) {
        vol = vol <= 1 ? vol * 100 : vol
        if (vol > 20) this._currentVolumen = vol
        this.player.setVolume(vol)
    }

    /**
     *
     * @returns {*}
     */
    getVolume() {
        return this.player.getVolume
            ? player.getVolume().then(function(volume) {
                  return volume
              })
            : 0
    }

    /**
     *
     * @returns {any}
     */
    getMuted() {
        return this.getVolume() == 0
    }

    /**
     *
     * @returns {*}
     */
    getCurrentTime() {
        return this.player.getCurrentTime ? this.player.getCurrentTime() : 0
    }

    /**
     *
     * @returns {any|number}
     */
    getDuration() {
        return this.player.getDuration ? this.player.getDuration() : null
    }

    /**
     *
     */
    getBuffered() {
        var percent = (this.player.getVideoBytesLoaded() / this.player.getVideoBytesTotal()) * 100
        return (this.getDuration() * percent) / 100
    }

    /**
     *
     * @returns {any|string}
     */
    getSource() {
        return this.player.getVideoUrl()
    }

    getVideoUrl() {
        return this.player.getVideoUrl()
    }

    isBuffering() {
        return false
    }

    /**
     *
     * @returns {boolean}
     */
    isPaused() {
        return this.player.getPlayerState() != 1
    }

    stop() {
        this.pause()
        this.setCurrentTime(0)
    }

    destroy() {
        if (!this.player) return

        this.player.removeEventListener('abort', this.onStateChange)
        this.player.removeEventListener('canplay', this.onStateChange)
        this.player.removeEventListener('canplaythrough', this.onStateChange)
        this.player.removeEventListener('durationchange', this.onStateChange)
        this.player.removeEventListener('emptied', this.onStateChange)
        this.player.removeEventListener('ended', this.onStateChange)
        this.player.removeEventListener('error', this.onStateChange)
        this.player.removeEventListener('loadeddata', this.onStateChange)
        this.player.removeEventListener('loadedmetadata', this.onStateChange)
        this.player.removeEventListener('loadstart', this.onStateChange)
        this.player.removeEventListener('pause', this.onStateChange)
        this.player.removeEventListener('play', this.onStateChange)
        this.player.removeEventListener('progress', this.onStateChange)
        this.player.removeEventListener('seeking', this.onStateChange)
        this.player.removeEventListener('suspend', this.onStateChange)
        this.player.removeEventListener('timeupdate', this.onStateChange)
        this.player.removeEventListener('volumechange', this.onStateChange)
        this.player.removeEventListener('waiting', this.onStateChange)
    }
}

export default {
    canPlay(url) {
        return true
    },

    canCreate() {
        return true
    },

    create(options) {
        return new VideoPlayer(options)
    },

    loadAPI(callback) {
        callback()
    }
}
