import Emitter from 'component-emitter'
import MediaEvents from 'vue'

class YouTubePlayer {

    constructor({ url, ...playerVars }) {
        this.videoId = url.substr(url.lastIndexOf('/') + 1).replace('?v=', '')
        this.playerVars = playerVars
        this.element = document.createElement('div')

        this.dispatcher = new Emitter()
        this.setup()
    }

    setup() {
        const self = this
        this.player = new YT.Player(this.playerVars.id, {
            videoId: this.videoId,
            playerVars: this.playerVars,
            events: {
                onReady() {
                    self.onReady()
                },

                onStateChange(event) {
                    self.onStateChange(event)
                },

                onError(event) {
                    self.dispatcher.emit('error', event)
                }
            }
        })
    }

    canPlayType() {
        return true
    }

    onReady() {
        this.ready = true
        this.dispatcher.emit('ready')
    }

    onStateChange(event) {
        let typeEvent = event.type || ''

        switch (event.data) {
            case YT.PlayerState.ENDED:
                typeEvent = MediaEvents.ended
                break
            case YT.PlayerState.PLAYING:
                typeEvent = MediaEvents.play
                break
            case YT.PlayerState.PAUSED:
                typeEvent = MediaEvents.pause
                break
            case YT.PlayerState.CUED:
                typeEvent = MediaEvents.seeking
                break
        }

        this.dispatcher.emit('stateChange', typeEvent, event)
    }

    /**
     *
     */
    play() {
        this.player.playVideo()
    }

    /**
     *
     */
    replay() {
        this.player.playVideo()
    }

    /**
     *
     */
    pause() {
        this.player.pauseVideo()
    }

    /**
     *
     * @param mute
     */
    mute() {
        this.setVolume(0)
    }

    unmute() {
        this.setVolume(this._currentVolumen)
    }

    /**
     *
     * @param seconds
     * @param disableSeekAhead
     */
    seekTo(seconds, disableSeekAhead = false) {
        this.player.seekTo(seconds, !disableSeekAhead)
    }

    /**
     *
     */
    load() {}

    /**
     *
     * @param src
     */
    setSrc(src) {
        this.player.loadVideoById({ videoId: src, startSeconds: 0 })
    }

    /**
     *
     * @param vol
     */
    setVolume(vol) {
        vol = vol <= 1 ? vol * 100 : vol
        if (vol > 20) this._currentVolumen = vol
        this.player.setVolume(vol)
    }

    /**
     *
     * @returns {*}
     */
    getVolume() {
        return this.player.getVolume ? this.player.getVolume() / 100 : 0
    }

    /**
     *
     * @returns {any}
     */
    getMuted() {
        return this.getVolume() == 0
    }

    /**
     *
     * @returns {*}
     */
    getCurrentTime() {
        return this.player.getCurrentTime ? this.player.getCurrentTime() : 0
    }

    /**
     *
     * @returns {any|number}
     */
    getDuration() {
        return this.player.getDuration ? this.player.getDuration() : null
    }

    /**
     *
     */
    getBuffered() {
        var percent = (this.player.getVideoBytesLoaded() / this.player.getVideoBytesTotal()) * 100
        return (this.getDuration() * percent) / 100
    }

    /**
     *
     * @returns {any|string}
     */
    getSource() {
        return this.player.getVideoUrl()
    }

    getVideoUrl() {
        return this.player.getVideoUrl()
    }

    isBuffering() {
        return false
    }
    /**
     *
     * @returns {boolean}
     */
    isPaused() {
        return this.player.getPlayerState() != 1
    }

    stop() {
        this.pause()
        this.setCurrentTime(0)
    }

    destroy() {
        this.player.destroy()
    }
}


let delay = null

export default {

    canPlay(url) {
        return /youtube/.test(url)
    },
    
    canCreate() {
        console.log(window.YT)
        return (window.YT && YT.Player)
    },

    create(options) {
        return new YouTubePlayer(options)
    },

    loadAPI(callback) {
        if (delay) return

        var apiUrl = '//www.youtube.com/iframe_api?enablejsapi=1&html5=1'
        var scriptTags = document.getElementsByTagName('script')
        var i = scriptTags.length
        var scriptTag

        while (i) {
            i -= 1
            if (scriptTags[i].src === apiUrl) {
                return
            }
        }
        scriptTag = document.createElement('script')
        scriptTag.src = apiUrl
        scriptTags[0].parentNode.insertBefore(scriptTag, scriptTags[0])

        delay = setInterval( () => {
            if (this.canCreate()) {
                callback()
                clearInterval(delay)
            }
        },100)
    }
}
